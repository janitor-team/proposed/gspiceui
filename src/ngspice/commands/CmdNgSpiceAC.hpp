//**************************************************************************************************
//                                        CmdNgSpiceAC.hpp                                         *
//                                       ------------------                                        *
// Description : This class contains values associated with the NG-Spice AC command. It inherits   *
//               from the class CmdBase.                                                           *
// Started     : 2007-10-11                                                                        *
// Last Update : 2018-09-23                                                                        *
// Copyright   : (C) 2007-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDNGSAC_HPP
#define CMDNGSAC_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "gnucap/commands/CmdGnuCapAC.hpp"
#include "utility/CnvtType.hpp"

class CmdGnuCapAC;

//**************************************************************************************************

class CmdNgSpiceAC : public CmdBase
{
  private :

    bool  bValidate( void ) override;

  public :

    wxString    m_osStart;  // The sweep start value
    wxString    m_osStop;   // The sweep stop  value
    wxString    m_osStep;   // The sweep step  value
    eTypeScale  m_eScale;   // The sweep scale type

          CmdNgSpiceAC( void );
         ~CmdNgSpiceAC( );

    bool  bSetDefaults( void ) override;

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    CmdNgSpiceAC & operator = ( const CmdGnuCapAC & roCmdAC );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDNGSAC_HPP

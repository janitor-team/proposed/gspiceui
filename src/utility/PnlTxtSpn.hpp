//**************************************************************************************************
//                                         PnlTxtSpn.hpp                                           *
//                                        ---------------                                          *
// Description : This class extends wxPanel and can substitute for the wxWidgets library class     *
//               wxSpinCtrl. It adds some useful functionality :                                   *
//                - floating point or integer numbers can be displayed,                            *
//                - incrementing by orders of magnitude not just a constant,                       *
//                - variable step size incrementing.                                               *
// Started     : 2004-03-20                                                                        *
// Last Update : 2018-10-22                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLTXTSPN_HPP
#define PNLTXTSPN_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "utility/CnvtType.hpp"

// wxWidgets Includes

#include <wx/spinbutt.h>

// Local Constant Declarations

#define  SPN_MAXLEN        9  // Maximum chars. that may be entered into text ctrl.
#define  SPN_PERIOD_DEF   83  // The default spin button repeat period (in msec)
#define  SPN_PERIOD_MIN   20  // The minimum spin button repeat period (in msec)
#define  SPN_PERIOD_MAX  400  // The maximum spin button repeat period (in msec)

//**************************************************************************************************

class PnlTxtSpn : public wxPanel
{
  private :

    // Display objects
    wxTextCtrl    m_oTxtValue;
    wxSpinButton  m_oSbnValue;

    eTypeValue    m_eValType;     // The value type to be displayed
    double        m_dfDefValue;   // Default value
    double        m_dfMinValue;   // Minimum value
    double        m_dfMaxValue;   // Maximum value
    double        m_dfMinIncSz;   // Minimum increment size
    double        m_dfMaxIncSz;   // Maximum increment size

    static  uint  m_uiSpnPeriod;  // The interval between successive spin control updates

    void  DoLayout   ( void );

  public :

          PnlTxtSpn  ( void );
         ~PnlTxtSpn  ( );

    bool  bCreate    ( wxWindow * poWin, wxWindowID oWinID, int iWidth=-1 );
    bool  bIsCreated ( void )                { return( GetParent( )!=NULL ? true : false ); }

    bool  bClear     ( void );

    bool  bSetValType( eTypeValue eVType );
    bool  bSetRange  ( double dfMinValue, double dfMaxValue );
    bool  bSetIncSz  ( double dfMinIncSz, double dfMaxIncSz=-1.0 );
    bool  bSetValue  ( long liValue )        { return( bSetValue( (double) liValue ) ); }
    bool  bSetValue  ( double dfValue );
    bool  bSetValue  ( const wxString & rosValue );
    bool  bSetValDef ( double dfDefValue=NOVAL_DBL );
    bool  bSetValMin ( double dfMinValue )   { return( bSetRange(   dfMinValue, m_dfMaxValue ) ); }
    bool  bSetValMax ( double dfMaxValue )   { return( bSetRange( m_dfMinValue,   dfMaxValue ) ); }

    inline  eTypeValue eGetVType   ( void ) { return( m_eValType ); }
            long      liGetValue   ( void );
            double    dfGetValue   ( void );
    const wxString & rosGetValue   ( void );
    inline  double    dfGetValDef  ( void ) { return( m_dfDefValue ); }
    inline  double    dfGetValMin  ( void ) { return( m_dfMinValue ); }
    inline  double    dfGetValMax  ( void ) { return( m_dfMaxValue ); }
    inline  double    dfGetMinIncSz( void ) { return( m_dfMinIncSz ); }
    inline  double    dfGetMaxIncSz( void ) { return( m_dfMaxIncSz ); }

    static  bool      bSetSpnPeriod( uint uiPeriod );

    void  Print( const wxString & rosPrefix=wxT("  ") );

  private :

    // Event handlers
    void  OnTxtChar  ( wxKeyEvent     & roEvtKey );
    void  OnTxtMaxLen( wxCommandEvent & roEvtCmd );
    void  OnSbnInc   ( wxSpinEvent    & roEvtSpn );
    void  OnSbnDec   ( wxSpinEvent    & roEvtSpn );

    // Declare friend classes
    friend  class  PnlValue;

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum ePnlItemID
    {
      ID_TXTCTRL = 1,
      ID_SPINBTN,

      ID_UNUSED,

      ID_FST = ID_TXTCTRL,
      ID_LST = ID_SPINBTN
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // PNLTXTSPN_HPP

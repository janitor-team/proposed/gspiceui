//**************************************************************************************************
//                                         CmdGnuCapTR.hpp                                         *
//                                        -----------------                                        *
// Description : This class contains values associated with the GNU-Cap TRANSIENT command. It      *
//               inherits from the class CmdBase.                                                  *
// Started     : 2008-03-07                                                                        *
// Last Update : 2018-09-23                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDGCPTR_HPP
#define CMDGCPTR_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "ngspice/commands/CmdNgSpiceTR.hpp"
#include "utility/CnvtType.hpp"

class CmdNgSpiceTR;

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class CmdGnuCapTR : public CmdBase
{
  private :

    bool  bValidate( void ) override;

  public :

    wxString    m_osStart;  // The sweep start value
    wxString    m_osStop;   // The sweep stop  value
    wxString    m_osStep;   // The sweep step  value
    eTypeInitC  m_eInitC;   // The sweep scale type
    wxString    m_osTempC;  // The analysis temperature

          CmdGnuCapTR( void );
         ~CmdGnuCapTR( );

    bool  bSetDefaults( void ) override;

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    CmdGnuCapTR & operator = ( const CmdNgSpiceTR & roCmdTR );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDGCPTR_HPP

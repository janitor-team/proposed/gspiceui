//**************************************************************************************************
//                                          PrcGnuCap.hpp                                          *
//                                         ---------------                                         *
// Description : This class provides the interface to the GNU-Cap electronic circuit simulator.    *
// Started     : 2003-09-01                                                                        *
// Last Update : 2018-09-24                                                                        *
// Copyright   : (C) 2003-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PRCGNUCAP_HPP
#define PRCGNUCAP_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PrcSimEngBase.hpp"
#include "netlist/SimnGnuCap.hpp"
#include "gnucap/commands/CmdGnuCapOP.hpp"
#include "gnucap/commands/CmdGnuCapDC.hpp"
#include "gnucap/commands/CmdGnuCapAC.hpp"
#include "gnucap/commands/CmdGnuCapTR.hpp"
#include "gnucap/commands/CmdGnuCapFO.hpp"
#include "gnucap/commands/CmdGnuCapPR.hpp"
#include "utility/CnvtType.hpp"

// wxWidgets Includes

#include <wx/config.h>

//**************************************************************************************************

class PrcGnuCap : public PrcSimEngBase
{
  private:

    // The label for the X axis variable (missing in GNU-Cap OP & DC analysis)
    wxString  m_osSigSrcName;

    // Functions to check / format the circuit description lines
//    bool  bFmtCpnts( ArrayComponent & roaCpnts ); ??? 17/08/2009

    // Format the results file
    bool  bFmtGeneral  ( void );
    bool  bFmtColLabels( void );
    bool  bFmtDataLines( void );
    bool  bFmtResultsFO( void );

  public:

                   PrcGnuCap( void );
                  ~PrcGnuCap( );

    // Create the process argument list
    virtual  bool  bMakeArgLst( SimnBase & roSimn ) override;

    // Format the results file
    virtual  bool  bFmtResults( void ) override;
};

//**************************************************************************************************

#endif // PRCGNUCAP_HPP

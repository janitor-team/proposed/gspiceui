v 20050313 1
C 3700 750 1 90 0 resistor-2.sym
{
T 3450 1400 5 10 1 1 180 0 1
refdes=R1
T 3500 1150 5 10 1 1 180 0 1
value=100
}
C 2550 2500 1 0 0 resistor-2.sym
{
T 2900 2750 5 10 1 1 0 0 1
refdes=R2
T 2900 2300 5 10 1 1 0 0 1
value=1K
}
C 4850 2500 1 0 0 resistor-2.sym
{
T 5200 2750 5 10 1 1 0 0 1
refdes=R3
T 5200 2300 5 10 1 1 0 0 1
value=1K
}
C 3450 2000 1 180 0 capacitor-1.sym
{
T 2700 1900 5 10 1 1 0 0 1
refdes=C1
T 3300 2000 5 10 1 1 180 0 1
value=1p
}
N 3450 2600 4850 2600 4
N 5750 1800 6250 1800 4
N 5750 2600 5900 2600 4
N 5900 2600 5900 1800 4
N 3450 1800 4250 1800 4
N 3600 1650 3600 1800 4
N 4250 1200 4050 1200 4
N 4050 1200 4050 2600 4
N 2550 1800 2300 1800 4
N 900 600 6250 600 4
N 2550 2600 2450 2600 4
N 2450 2600 2450 1800 4
N 5750 1200 5750 600 4
C 4500 300 1 0 0 gnd-1.sym
N 3600 750 3600 600 4
C 1100 2100 1 270 0 vac-1.sym
{
T 1600 2200 5 10 1 1 0 0 1
refdes=Vin
T 1300 1300 5 10 0 1 0 0 1
value=dc 0 ac 1
}
C 6150 1650 1 270 0 resistor-2.sym
{
T 6450 1250 5 10 1 1 0 0 1
refdes=Rout
T 6900 1100 5 10 1 1 180 0 1
value=100K
}
T 3600 4700 9 14 1 0 0 4 1
Ideal Single Pole Active RC Phase Shifter
C 600 600 1 0 0 vdc-1.sym
{
T 1300 1200 5 10 1 1 0 0 1
refdes=Vbias
T 1350 1000 5 10 1 1 0 0 1
value=2.5V
}
N 900 1800 1100 1800 4
T 3550 4350 9 10 1 0 0 4 1
Last Updated on 05/08/2005
C 4250 1100 1 0 0 vcvs-1.sym
{
T 4850 2000 5 12 1 1 0 0 1
refdes=E1
T 4250 1100 5 10 0 0 0 0 1
value=10000
}
N 6250 1800 6250 1650 4
N 6250 750 6250 600 4
T 3650 3600 9 8 1 0 0 4 5
For R1 =   1K Ohm and Vin(freq) = 10.0 GHz phase at Rout =   1.8 Degree
For R1 = 100 Ohm and Vin(freq) = 10.0 GHz phase at Rout =  18.1 Degree
For R1 =   50 Ohm and Vin(freq) = 10.0 GHz phase at Rout = 35.3 Degree
For R1 =   10 Ohm and Vin(freq) = 10.0 GHz phase at Rout = 115.7 Degree
For R1 =     1 Ohm and Vin(freq) = 10.0 GHz phase at Rout = 172.8 Degree

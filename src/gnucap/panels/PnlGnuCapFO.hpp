//**************************************************************************************************
//                                         PnlGnuCapFO.hpp                                         *
//                                        -----------------                                        *
// Description : This class derives from the PnlAnaBase base class and provides a GUI for the user *
//               to configure a Fourier Analysis for GNU-Cap electronic circuit simulation engine. *
// Started     : 2004-04-26                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLGNUCAPFO_HPP
#define PNLGNUCAPFO_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PnlAnaBase.hpp"
#include "netlist/SimnGnuCap.hpp"
#include "gnucap/dialogs/DlgGnuCapCfgGEN.hpp"

// wxWidgets Library Includes

#include <wx/config.h>

//**************************************************************************************************

class PnlGnuCapFO : public PnlAnaBase
{
  private :

    // Output Parameters
    wxStaticBox   m_oSbxOutputs;
//  wxCheckBox    m_oCbxMag;        ??? 2018-09-23 MSW Already defined in the class PnlAnaBase
//  wxCheckBox    m_oCbxPhase;      ??? 2018-09-23 MSW Already defined in the class PnlAnaBase
//  wxCheckBox    m_oCbxMagDb;      ??? 2018-09-23 MSW Already defined in the class PnlAnaBase
    wxCheckBox    m_oCbxMagRel;

    // Generator configuration
    DlgGnuCapCfgGEN  m_oDlgCfgGEN;

    void  Create       ( void );
    void  CreateSigSrc ( void ) override;
    void  CreateOutputs( void );

    void  GetConfig( void );
    void  SetConfig( void );

  public :

    explicit  PnlGnuCapFO( wxWindow * poWin );
             ~PnlGnuCapFO( );

    bool  bClear( void ) override;

    bool  bLoad( SimnGnuCap & roSimn );
    bool  bSave( SimnGnuCap & roSimn );

    // Event handlers
    void  OnBtnSetup( wxCommandEvent & roEvtCmd );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum ePnlItemID
    {
      ID_BTN_SETUP = PnlAnaBase::ID_LST+1,

      ID_CBX_MAG,
      ID_CBX_PHASE,
      ID_CBX_MAGDB,
      ID_CBX_MAGREL,

      ID_UNUSED,      // Assigned to controls for which events are not used

      ID_FST = ID_BTN_SETUP,
      ID_LST = ID_CBX_MAGREL
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // PNLGNUCAPFO_HPP

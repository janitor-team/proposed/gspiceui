//**************************************************************************************************
//                                        AppPrcBase.cpp                                           *
//                                       ----------------                                          *
// Started     : 2018-10-23                                                                        *
// Last Update : 2018-10-24                                                                        *
// Copyright   : (C) 2018 MSWaters                                                                 *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "AppPrcBase.hpp"

//**************************************************************************************************

// Tell wxWidgets how to create an instance of this application class
wxIMPLEMENT_APP( AppPrcBase );

// Define a structure declaring the command line syntax
static  const  wxCmdLineEntryDesc  tCmdLnDesc[] =
{
#if wxCHECK_VERSION( 3,0,0 )
  { wxCMD_LINE_SWITCH, "h", ""    , "", wxCMD_LINE_VAL_NONE  , wxCMD_LINE_OPTION_HELP      },
  { wxCMD_LINE_OPTION, "b", "bin" , "", wxCMD_LINE_VAL_STRING, wxCMD_LINE_OPTION_MANDATORY },
  { wxCMD_LINE_OPTION, "a", "args", "", wxCMD_LINE_VAL_STRING, 0                           },
#else
  { wxCMD_LINE_SWITCH, wxT("h"), wxT("")    , wxT(""), wxCMD_LINE_VAL_NONE  , wxCMD_LINE_OPTION_HELP      },
  { wxCMD_LINE_OPTION, wxT("b"), wxT("bin") , wxT(""), wxCMD_LINE_VAL_STRING, wxCMD_LINE_OPTION_MANDATORY },
  { wxCMD_LINE_OPTION, wxT("a"), wxT("args"), wxT(""), wxCMD_LINE_VAL_STRING, 0                           },
#endif
  { wxCMD_LINE_NONE }
};

//**************************************************************************************************

bool  AppPrcBase::OnInit( void )
{
  wxCmdLineParser  oCmdLn;
  wxString         osBin, osArgs;

  oCmdLn.SetDesc( tCmdLnDesc );                                    // Set the command set
  oCmdLn.SetCmdLine( argc, argv );                                 // Set the command line
  if( oCmdLn.Parse( false ) != 0 ) { Usage( ); return( false ); }  // Try to parse the command line
  if( oCmdLn.Found( wxT("h") ) )   { Usage( ); return( false ); }  // Process "-h" switch if found
  oCmdLn.Found( wxT("b"), &osBin  );                               // Get the binary file to execute
  oCmdLn.Found( wxT("a"), &osArgs );                               // Get any arguments to binary
  m_oPrcBase.bSetBinFile( osBin );                                 // Set the binary to run
  if( osArgs.Len( ) > 0 ) m_oPrcBase.bSetArgLst( osArgs );         // Set the argument list

  return( true );
}

//**************************************************************************************************

int  AppPrcBase::MainLoop( )
{
  wxString  os1;

  // Display the utility banner
  std::cout << "\n  PrcBase Class Test Utility"
            << "\n   Version 1.00 2018-10-25)\n";

  std::cout << "\noPrcBase.Print( ) :\n";
  m_oPrcBase.Print( );

/*  // Set the working directory
  std::cout << "\nwxGetCwd( )              : " << wxGetCwd( );
  os1 = wxT("/home/msw/tmp");
  std::cout << "\nSet CWD to " << os1 << " : ";
  if( m_oPrcBase.bSetCwd( os1 ) ) std::cout << "Success\n";
  else                            std::cout << "Failure\n";
  std::cout << "m_oPrcBase.rosGetCwd( )  : " << m_oPrcBase.rosGetCwd( ) << '\n';
  std::cout << "wxGetCwd( )              : " << wxGetCwd( ) << "\n\n";
*/
  // Display then execute the command line
  std::cout << "Execute :\n" << m_oPrcBase.roGetBinFile( ).GetFullPath( ) << ' '
            << m_oPrcBase.rosGetArgLst( ) << "\n\n";
  m_oPrcBase.bExecAsync( );

  // Display result of command line execution
  std::cout << "Results :\n";
  m_oPrcBase.PrintOutput( );

  std::cout << '\n';

  return( EXIT_SUCCESS );
}

//**************************************************************************************************

void  AppPrcBase::Usage( void )
{
  std::cout << "\nUsage   : " << GetAppName( ) << " [-OPTIONS] [FILE]"
            << "\nOptions : -h      : Print usage (this message)"
            << "\n          -b BIN  : Set the binary file to execute"
            << "\n          -a ARGS : Set the arguments to give the binary"
            << "\n\n";
}

//**************************************************************************************************

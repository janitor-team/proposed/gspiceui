//**************************************************************************************************
//                                         AppConfig.cpp                                           *
//                                        ---------------                                          *
// Started     : 2016-09-27                                                                        *
// Last Update : 2018-10-24                                                                        *
// Copyright   : (C) 2014-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "AppConfig.hpp"

//**************************************************************************************************

// Tell wxWidgets how to create an instance of this application class
wxIMPLEMENT_APP( AppConfig );

// Define a structure declaring the command line syntax
static  const  wxCmdLineEntryDesc  tCmdLnDesc[] =
{
#if wxCHECK_VERSION( 3,0,0 )
  { wxCMD_LINE_SWITCH, "h", "help"  , "", wxCMD_LINE_VAL_NONE  , wxCMD_LINE_OPTION_HELP },
  { wxCMD_LINE_OPTION, "r", "rcfile", "", wxCMD_LINE_VAL_STRING, 0                      },
#else
  { wxCMD_LINE_SWITCH, wxT("h"), wxT("help")  , wxT(""), wxCMD_LINE_VAL_NONE  , wxCMD_LINE_OPTION_HELP },
  { wxCMD_LINE_OPTION, wxT("r"), wxT("rcfile"), wxT(""), wxCMD_LINE_VAL_STRING, 0                      },
#endif
  { wxCMD_LINE_NONE }
};

//**************************************************************************************************

bool  AppConfig::OnInit( void )
{
  wxCmdLineParser  oCmdLn;
  wxFileName       oRcFile;
  wxString         os1;

  oCmdLn.SetDesc( tCmdLnDesc );                                    // Set the command set
  oCmdLn.SetCmdLine( argc, argv );                                 // Set the command line
  if( oCmdLn.Parse( false ) != 0 ) { Usage( ); return( false ); }  // Try to parse the command line
  if( oCmdLn.Found( wxT("h") ) )   { Usage( ); return( false ); }  // Process "-h" switch if found
  if( ! oCmdLn.Found( wxT("r"), &os1 ) )                           // Process "-r" switch if found
    os1 = wxT("~/.gspiceui.conf");

  // Process the proposed configuration file name
  oRcFile = os1;
  oRcFile.MakeAbsolute( );
  if( ! oRcFile.FileExists( ) )
  {
    std::cout << "The given configuration file doesn't exist : " << os1 << '\n';
    return( false );
  }
  if( ! oRcFile.IsFileReadable( ) )
  {
    std::cout << "Don't have read access to the configuration file : " << os1 << '\n';
    return( false );
  }

  // Configure and create the global configuration object
  if( ! m_oConfig.bOpen( oRcFile.GetFullPath( ) ) )
  {
    std::cout << "Couldn't open the configuration file : " << os1 << '\n';
    return( false );
  }

  return( true );
}

//**************************************************************************************************

int  AppConfig::MainLoop( )
{
  // Display the utility banner
  std::cout << "\n  Config Class Test Utility"
            << "\n  Version 0.51 2018-10-24)\n";

  std::cout << "\noConfig.Print( ) :\n";
  m_oConfig.Print( );

  std::cout << "\n";

  return( EXIT_SUCCESS );
}

//**************************************************************************************************

void  AppConfig::Usage( void )
{
  std::cout << "\nUsage   : " << GetAppName( ) << " [-OPTIONS] [FILE]"
            << "\nOptions : -h        : Print usage (this message)"
            << "\n          -r RCFILE : Specify a configuration file"
            << "\n                      RCFILE = ~/.gspiceui.conf (default)"
            << "\n\n";
}

//**************************************************************************************************

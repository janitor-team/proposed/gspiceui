//**************************************************************************************************
//                                        PnlNgSpiceDC.hpp                                         *
//                                       ------------------                                        *
// Description : This class derives from the PnlAnaBase base class and provides a GUI for the user *
//               to configure a DC Analysis for the NG-Spice electronic circuit simulation engine. *
// Started     : 2004-05-08                                                                        *
// Last Update : 2018-10-04                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLNGSPICEDC_HPP
#define PNLNGSPICEDC_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PnlAnaBase.hpp"
#include "netlist/SimnNgSpice.hpp"

//**************************************************************************************************

class PnlNgSpiceDC : public PnlAnaBase
{
  private :

    void  Create( void );

    void  InitSwpUnits( void );

  public :

    explicit  PnlNgSpiceDC( wxWindow * poWin );
             ~PnlNgSpiceDC( );

    bool  bClear( void ) override;

    bool  bLoad( SimnNgSpice & roSimn );
    bool  bSave( SimnNgSpice & roSimn );

    // Event handlers
    void  OnSrcName( wxCommandEvent & roEvtCmd );

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // PNLNGSPICEDC_HPP

//**************************************************************************************************
//                                         SimnNgSpice.hpp                                         *
//                                        -----------------                                        *
// Description : A class to contain the values required to define a NG-Spice simulation.           *
// Started     : 2008-05-07                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef SIMNNGSPICE_HPP
#define SIMNNGSPICE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/SimnBase.hpp"
#include "ngspice/commands/CmdNgSpiceOPT.hpp"
#include "ngspice/commands/CmdNgSpiceDC.hpp"
#include "ngspice/commands/CmdNgSpiceAC.hpp"
#include "ngspice/commands/CmdNgSpiceTR.hpp"
#include "ngspice/commands/CmdNgSpicePR.hpp"
#include "netlist/SimnGnuCap.hpp"

class SimnGnuCap;

//**************************************************************************************************

class SimnNgSpice : public SimnBase
{
  public :

    CmdNgSpiceDC       m_oCmdDC;
    CmdNgSpiceAC       m_oCmdAC;
    CmdNgSpiceTR       m_oCmdTR;

    CmdNgSpiceOPT      m_oCmdOPT;
    CmdNgSpicePR       m_oCmdPR;
    CpntNgSpiceIndSrc  m_oCpntIndSrc;

  private :

    // Function to extract information from the circuit description
    bool  bLoadSimEng ( void ) override;
    bool  bLoadSimCmds( void ) override;
    bool  bLoadSigSrc ( void ) override;

  public :

          SimnNgSpice( void );
         ~SimnNgSpice( );

    bool  bClear     ( void ) override;
    bool  bClrCmds   ( void ) override;
    bool  bClrTstPts ( void ) override;

    bool  bValidate  ( void ) override;

    bool  bLoad      ( void ) override;
    bool  bSave      ( void ) override;

    bool  bSetAnaType( eTypeCmd eAnaType ) override { return( m_oCmdPR.bSetAnaType( eAnaType ) ); }

    bool  bAddTstNode( const wxString & rosName ) override;
    bool  bAddTstCpnt( const wxString & rosName ) override;

          eTypeCmd           eGetAnaType ( void ) const override { return( m_oCmdPR.eGetAnaType());}
    const wxArrayString & rosaGetTstNodes( void ) const override { return( m_oCmdPR.m_osaNodes   );}
    const wxArrayString & rosaGetTstCpnts( void ) const override { return( m_oCmdPR.m_osaCpnts   );}

    SimnNgSpice & operator = ( const SimnGnuCap & roSimn );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // SIMNNGSPICE_HPP

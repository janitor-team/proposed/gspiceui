//**************************************************************************************************
//                                        CmdNgSpiceDC.hpp                                         *
//                                       ------------------                                        *
// Description : This class contains values associated with the NG-Spice DC command. It inherits   *
//               from the class CmdBase.                                                           *
// Started     : 2006-08-23                                                                        *
// Last Update : 2018-09-23                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDNGSDC_HPP
#define CMDNGSDC_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "gnucap/commands/CmdGnuCapOP.hpp"
#include "gnucap/commands/CmdGnuCapDC.hpp"
#include "utility/CnvtType.hpp"

class CmdGnuCapOP;
class CmdGnuCapDC;

//**************************************************************************************************

class CmdNgSpiceDC : public CmdBase
{
  private :

    bool  bValidate( void ) override;

  public :

    wxString  m_osStart;   // Sweep start value
    wxString  m_osStop;    // Sweep stop  value
    wxString  m_osStep;    // Sweep step  value
    wxString  m_osSource;  // Sweep source

             CmdNgSpiceDC( void );
            ~CmdNgSpiceDC( );

    bool  bSetDefaults( void ) override;

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    CmdNgSpiceDC & operator = ( const CmdGnuCapOP & roCmdOP );
    CmdNgSpiceDC & operator = ( const CmdGnuCapDC & roCmdDC );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDNGSDC_HPP

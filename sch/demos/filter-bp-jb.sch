v 20130925 2
C 40000 40000 0 0 0 title-A4.sym
C 41700 45600 1 180 0 resistor-2.sym
{
T 41150 45650 5 10 1 1 0 0 1
refdes=Rs
T 41150 45200 5 10 1 1 0 0 1
value=50
}
N 44800 45500 45200 45500 4
N 40700 45500 40800 45500 4
C 45700 44200 1 0 0 gnd-1.sym
C 40500 45400 1 270 0 voltage-2.sym
{
T 40200 44900 5 10 1 1 0 0 1
refdes=Vs
T 40600 44700 5 10 0 1 180 0 1
value=DC 0V AC 1V
}
T 45650 40900 9 16 1 0 0 0 1
Resonant Band Pass Filter
C 51100 44500 1 90 0 resistor-2.sym
{
T 51400 45150 5 10 1 1 180 0 1
refdes=Ro
T 51400 44900 5 10 1 1 180 0 1
value=50
}
C 46500 44500 1 90 0 coil-2.sym
{
T 46000 44700 5 10 0 0 90 0 1
device=COIL
T 46650 45050 5 10 1 1 0 0 1
refdes=L2
T 45800 44700 5 10 0 0 90 0 1
symversion=0.1
T 46550 44800 5 10 1 1 0 0 1
value=88nH
}
N 46100 45500 46600 45500 4
N 47500 45500 47900 45500 4
T 40350 41150 9 10 1 0 0 0 11
The insertion loss is about 6dB.

The pass band width is about 6MHz.
The pass band roll-off varies with C1 & C4.
As C1 & C4 increase the bandwidth decreases.

The pass band center frequency varies with L1, C2 & C3.
For constant fc, as C2 & C3 increase L1 must decrease.
If L1 =    85nH then fc = 133MHz.
If L1 = 220nH then fc =   81MHz.

N 50600 45100 50600 44500 4
N 49200 45100 49200 44500 4
N 40700 44500 51000 44500 4
N 42100 44500 42100 45100 4
N 43500 45100 43500 44500 4
C 41700 45100 1 0 0 tline.sym
{
T 42600 45500 5 10 0 1 0 0 1
device=T-Line
T 42750 45050 5 10 1 1 0 0 1
refdes=Ts
T 42250 45800 5 10 1 1 0 0 1
value=Z0=50 TD=1nS
}
C 48800 45100 1 0 0 tline.sym
{
T 49700 45500 5 10 0 1 0 0 1
device=T-Line
T 49850 45050 5 10 1 1 0 0 1
refdes=To
T 49300 45800 5 10 1 1 0 0 1
value=Z0=50 TD=1nS
}
N 51000 45500 51000 45400 4
T 45200 40100 9 10 1 0 0 0 1
1
T 46500 40100 9 10 1 0 0 0 1
1
T 45200 40400 9 10 1 0 0 0 1
filter-bp-jb.sch
T 48900 40400 9 10 1 0 0 0 1
2015 - 03 - 27
T 48900 40100 9 10 1 0 0 0 1
Mike Waters
N 40700 45400 40700 45500 4
C 43900 45300 1 0 0 capacitor-1.sym
{
T 44100 46000 5 10 0 0 0 0 1
device=CAPACITOR
T 44200 45600 5 10 1 1 0 6 1
refdes=C0
T 44100 46200 5 10 0 0 0 0 1
symversion=0.1
T 44500 45600 5 10 1 1 0 0 1
value=5pF
}
C 45200 45300 1 0 0 capacitor-1.sym
{
T 45400 46000 5 10 0 0 0 0 1
device=CAPACITOR
T 45500 45600 5 10 1 1 0 6 1
refdes=C2
T 45400 46200 5 10 0 0 0 0 1
symversion=0.1
T 45800 45600 5 10 1 1 0 0 1
value=22pF
}
C 46600 45300 1 0 0 capacitor-1.sym
{
T 46800 46000 5 10 0 0 0 0 1
device=CAPACITOR
T 46900 45600 5 10 1 1 0 6 1
refdes=C3
T 46800 46200 5 10 0 0 0 0 1
symversion=0.1
T 47200 45600 5 10 1 1 0 0 1
value=22pF
}
C 47900 45300 1 0 0 capacitor-1.sym
{
T 48100 46000 5 10 0 0 0 0 1
device=CAPACITOR
T 48200 45600 5 10 1 1 0 6 1
refdes=C5
T 48100 46200 5 10 0 0 0 0 1
symversion=0.1
T 48500 45600 5 10 1 1 0 0 1
value=5pF
}
C 45200 44500 1 90 0 capacitor-1.sym
{
T 44500 44700 5 10 0 0 90 0 1
device=CAPACITOR
T 45100 45100 5 10 1 1 0 0 1
refdes=C1
T 44300 44700 5 10 0 0 90 0 1
symversion=0.1
T 45100 44800 5 10 1 1 180 6 1
value=8pF
}
C 47900 44500 1 90 0 capacitor-1.sym
{
T 47200 44700 5 10 0 0 90 0 1
device=CAPACITOR
T 47800 45100 5 10 1 1 0 0 1
refdes=C4
T 47000 44700 5 10 0 0 90 0 1
symversion=0.1
T 47800 44800 5 10 1 1 180 6 1
value=8pF
}
N 45000 45500 45000 45400 4
N 47700 45500 47700 45400 4
T 46900 41900 9 10 1 0 0 0 1
This filter was originally designed by John Beard.
T 40400 40400 9 10 1 0 0 0 3
The circuit can be simulated using
GNU-Cap but not NG-Spice
(last tested 2015-03-27)

//**************************************************************************************************
//                                        AppPnlValue.hpp                                          *
//                                       -----------------                                         *
// Description : This is a wxWidgets application designed to test and debug the PnlValue class.    *
// Started     : 2014-11-17                                                                        *
// Last Update : 2015-01-05                                                                        *
// Copyright   : (C) 2014-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

// System Includes

#include <vector>

// Application Includes

#include "utility/PnlLblTxt.hpp"
#include "utility/PnlValue.hpp"

//**************************************************************************************************
//                                    Class App_PnlValue
//**************************************************************************************************

class AppPnlValue : public wxApp
{
  public :

    virtual bool OnInit( );
};

//**************************************************************************************************
//                                    Class FrmMain
//**************************************************************************************************

class FrmMain : public wxFrame
{
  private :

    PnlLblTxt  m_oTxtInput;
    PnlValue   m_oPnlValue;
    PnlLblTxt  m_oTxtOutput;

    wxButton   m_oBtnNext;
    wxButton   m_oBtnRead;
    wxButton   m_oBtnQuit;

    std::vector<wxString>  oVecTests;

  public :

    FrmMain( const wxString & osTitle, const wxPoint & oPosn, const wxSize & oSize );

  private :

    void  Create  ( void );
    void  DoLayout( void );

    // Event handlers
    void  OnNext ( wxCommandEvent & roEvent );
    void  OnRead ( wxCommandEvent & roEvent );
    void  OnAbout( wxCommandEvent & roEvent );
    void  OnExit ( wxCommandEvent & roEvent );

    // In order to be able to react to a menu command, it must be given a unique identifier such as
    // a const or an enum.
    enum eDlgItemID
    {
      ID_TXT_INPUT = 1,
      ID_VAL_TEST  ,
      ID_TXT_OUTPUT,
      ID_BTN_NEXT,
      ID_BTN_READ,
      ID_BTN_QUIT
    };

    wxDECLARE_EVENT_TABLE( );
};

//**************************************************************************************************

//**************************************************************************************************
//                                          DlgPrefs.hpp                                           *
//                                         --------------                                          *
// Description : This dialogue is used to enter applications preferences.                          *
// Started     : 2006-10-17                                                                        *
// Last Update : 2018-10-28                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef DLGPREFS_HPP
#define DLGPREFS_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "CmdLinePcr.hpp"
#include "utility/PnlValue.hpp"
#include "utility/TextCtrl.hpp"
#include "utility/PnlLblCho.hpp"
#include "utility/PnlLblTxt.hpp"
#include "utility/PnlTxtSpn.hpp"
#include "process/PrcGNetList.hpp"

// wxWidgets Includes

#include <wx/config.h>

//**************************************************************************************************

class DlgPrefs : public wxDialog
{
  private :

    // Display controls
    PnlLblCho   m_oChoEdaSuite;
    PnlLblCho   m_oChoDataViewer;
    PnlLblCho   m_oChoTmpFileMgt;
    PnlLblCho   m_oChoFrmLayout;
    PnlLblCho   m_oChoPhaseUnits;
    PnlLblCho   m_oChoPrecision;
    PnlLblCho   m_oChoGuileProc;
    PnlValue    m_oPnlNbkMaxLns;
    PnlValue    m_oPnlSpnPeriod;
    PnlValue    m_oPnlToolTipDly;
    wxCheckBox  m_oCbxToolTips;
    wxCheckBox  m_oCbxSyncSigSrcs;
    wxCheckBox  m_oCbxSyncTemps;
    wxCheckBox  m_oCbxKeepNetLst;
    wxCheckBox  m_oCbxVerboseMode;
    wxCheckBox  m_oCbxIncludeMode;
    wxCheckBox  m_oCbxEmbedMode;
    wxCheckBox  m_oCbxNoMungeMode;
    wxCheckBox  m_oCbxAutoRegen;
//  wxCheckBox  m_oCbxSubcktCpnts;  // ??? MSW 2015-04-18 Include sub-circuit components in Components list
//  PnlLblTxt   m_oTxtBinGschem;    // ??? MSW 2014-03-11 Allow binary names to be specified
//  PnlLblTxt   m_oTxtBinGnetlst;
//  PnlLblTxt   m_oTxtBinNgspice;
//  PnlLblTxt   m_oTxtBinGnucap;
//  PnlLblTxt   m_oTxtBinGaw;
//  PnlLblTxt   m_oTxtBinGwave;
//  PnlLblTxt   m_oTxtBinCalc;
//  PnlLblTxt   m_oTxtDocNgSpice;
//  PnlLblTxt   m_oTxtDocGnuCap;

    // Button controls
    wxButton    m_oBtnOk;
    wxButton    m_oBtnCancel;

    // Object initialization functions
    void  Initialize( void );
    void  Create    ( void );
    void  ToolTips  ( void );
    void  DoLayout  ( void );

    void  Load( void );
    void  Save( void );

  public :

    explicit  DlgPrefs( wxWindow * poWin );
             ~DlgPrefs( );

    bool  bClear( void );

  private :

    // Event handlers
    void  OnBtnOk    ( wxCommandEvent & roEvtCmd );
    void  OnBtnCancel( wxCommandEvent & roEvtCmd );

    // Declare friend classes
    friend  class  FrmMain;

    // In order to be able to react to a menu command, it must be given a unique identifier such as
    // a const or an enum.
    enum eDlgItemID
    {
      ID_CHO_SIMENG,
      ID_CHO_EDASUITE,
      ID_CHO_DATAVIEWER,
      ID_CHO_TMPFILEMGT,
      ID_CHO_FRMLAYOUT,
      ID_CHO_PHASEUNITS,
      ID_CHO_PRECISION,
      ID_CHO_GUILEPROC,

      ID_VAL_NBKMAXLNS,
      ID_VAL_SPINPERIOD,
      ID_VAL_TOOLTIPDLY,

      ID_CBX_TOOLTIPS,
      ID_CBX_SYNCSIGSRCS,
      ID_CBX_SYNCTEMPS,
      ID_CBX_KEEPNETLST,
      ID_CBX_VERBOSEMODE,
      ID_CBX_INCLUDEMODE,
      ID_CBX_EMBEDMODE,
      ID_CBX_NOMUNGEMODE,
      ID_CBX_AUTOREGEN,

      ID_TXT_GSCHEM,
      ID_TXT_GNETLST,
      ID_TXT_NGSPICE,
      ID_TXT_GNUCAP,
      ID_TXT_GAW,
      ID_TXT_GWAVE,
      ID_TXT_CALC,

      ID_BTN_OK,
      ID_BTN_CANCEL,

      ID_UNUSED,      // Assigned to controls for which events are not used
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // DLGPREFS_HPP

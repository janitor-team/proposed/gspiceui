//**************************************************************************************************
//                                          LblUnits.hpp                                           *
//                                         --------------                                          *
// Description : This class is intended to provide useful functionality for managing the units     *
//               associated with some variable. It is particular aimed at parameters to do with    *
//               electronics.                                                                      *
// Started     : 2014-03-01                                                                        *
// Last Update : 2018-10-19                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef LBLUNITS_HPP
#define LBLUNITS_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/UnitsBase.hpp"

//**************************************************************************************************

class LblUnits : public UnitsBase, public wxLabel
{
  private :

    eTypeUnits  m_eUnitsType;
    int         m_iUnitsExp;

  public :

          LblUnits( void );
         ~LblUnits( );

    bool  bCreate   ( wxWindow * poWin, wxWindowID oWinID, int iWidth=-1 );
    bool  bIsCreated( void ) override { return( wxLabel::GetParent( )!=NULL ? true : false ); }

    bool  bClear( void );

    bool  bSetUnitsType( eTypeUnits eUType );
    bool  bSetUnits    ( const wxString & rosUnits );
    bool  bSetUnits    ( int iExp );

          eTypeUnits   eGetUnitsType( void )          { return( m_eUnitsType ); }
    const wxString & rosGetUnits    ( void ) override;
          int          iGetUnits    ( void ) override { return( m_iUnitsExp ); };

    void  Print( const wxString & rosPrefix=wxT("  ") );
};

//**************************************************************************************************

#endif // LBLUNITS_HPP

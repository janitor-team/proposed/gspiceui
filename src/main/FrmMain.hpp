//**************************************************************************************************
//                                          FrmMain.hpp                                            *
//                                         -------------                                           *
// Description : The main frame used to contain the applications display objects.                  *
// Started     : 2003-08-18                                                                        *
// Last Update : 2018-10-22                                                                        *
// Copyright   : (C) 2003-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef FRMMAIN_HPP
#define FRMMAIN_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"

#include "main/FileTasks.hpp"
#include "main/HelpTasks.hpp"
#include "main/NbkTxtCtls.hpp"
#include "main/DlgPrefs.hpp"

#include "netlist/NetList.hpp"
#include "netlist/SimnNgSpice.hpp"
#include "netlist/SimnGnuCap.hpp"

#include "process/PrcGSchem.hpp"
#include "process/PrcNgSpice.hpp"
#include "process/PrcGnuCap.hpp"
#include "process/PrcGaw.hpp"
#include "process/PrcGWave.hpp"

#include "gnucap/NbkGnuCap.hpp"
#include "ngspice/NbkNgSpice.hpp"

// wxWidgets Includes

#include <wx/config.h>
#include <wx/process.h>
#include <wx/cursor.h>
#include <wx/tooltip.h>
#include <wx/gbsizer.h>
#include <wx/progdlg.h>

//**************************************************************************************************

class FrmMain : public wxFrame
{
  private :

    // Define an enumerated type for the different status bar panes
    enum ePaneType
    {
      ePANE_MESAGE = 0,   // Message strings
      ePANE_SIMENG = 1,   // Simulation engine
      ePANE_EDATLS = 2,   // EDA tool suite
      ePANE_WAVVWR = 3,   // Waveform data viewer

      ePANE_LAST = ePANE_WAVVWR
    };

    // Object attributes
    bool            m_bIsOpen;      // Flag indicating when the frame has closed
    uint            m_uiFrmLayout;  // Indicator of which main frame layout to use

    FileTasks       m_oFileTasks;   // Class to handler file operations
    HelpTasks       m_oHelpTasks;   // Class to handler help operations

    NetList         m_oNetLst;      // The netlist object
    SimnNgSpice     m_oSimnNgSp;    // NG-Spice simulation object
    SimnGnuCap      m_oSimnGCap;    // GNU-Cap  simulation object
    SimnBase      * m_poSimn;       // A pointer to the simulation object

    PrcSimEngBase * m_poPrcSimEng;  // Pointer to the simulator engine process
    PrcGSchem       m_oPrc_gschem;  // Process object to schematic capture
    PrcGaw          m_oPrc_gaw;     // Process object to view simulation results
    PrcGWave        m_oPrc_gwave;   // Process object to view simulation results

    // Display control objects
    NbkSimEngBase * m_poNbkSimEng;  // Pointer to the simulation engine notebook
    wxLabel         m_oLblNodes;    // Label above the test nodes list
    wxListBox       m_oLbxNodes;    // List of possible test nodes
    wxLabel         m_oLblCpnts;    // Label above the test components list
    wxListBox       m_oLbxCpnts;    // List of possible test components
    NbkTxtCtls      m_oNbkTxtCtls;  // Notebook containing the text controls

    // Object initialization functions
    void  Initialize  ( void );
    void  InitMenuBar ( void );
    void  InitToolBar ( void );
    void  InitLstBoxs ( void );
    void  InitNbkTCtls( void );
    void  InitStatBar ( void );
    void  InitToolTips( void );
    void  InitSimEng  ( void );
    void  InitLogFiles( void );
    void  InitPosnSize( void );
    void  DoLayout    ( void );

	  // Read / write simulation information from / to file
    bool  bSimnLoad  ( void );
    bool  bSimnReload( void );
    bool  bSimnSave  ( void );

    // Check that helper utilities are present & accounted for
    bool  bIsOkSimEng( PrcBase * poPrcSimEng );
    bool  bIsOkViewer( PrcBase * poPrcViewer );
    bool  bIsOkBinary( PrcBase * poProcess, const wxString & rosPurpose );

    void  LockGUI( bool bEnable );  // Lock GUI controls

  public :

    explicit  FrmMain( const wxApp * poApp=NULL );
             ~FrmMain( );

    bool  bIsOpen( void ) { return( m_bIsOpen ); }

    bool  bClear( void );  // Clear the object attributes

    // Display a dialogue containing an error message
    void  DlgErrMsg( const wxString & rosTitle=wxT(""), const wxString & rosMsg=wxT("") );

  private :

    // Event handlers
    void  OnOpen     ( wxCommandEvent & roEvtCmd   );
    void  OnImport   ( wxCommandEvent & roEvtCmd   );
    void  OnReload   ( wxCommandEvent & roEvtCmd   );
    void  OnClose    ( wxCommandEvent & roEvtCmd   );
    void  OnQuit     ( wxCommandEvent & roEvtCmd   );
    void  OnSimCreate( wxCommandEvent & roEvtCmd   );
    void  OnSimRun   ( wxCommandEvent & roEvtCmd   );
    void  OnSimStop  ( wxCommandEvent & roEvtCmd   );
    void  OnSchematic( wxCommandEvent & roEvtCmd   );
    void  OnViewData ( wxCommandEvent & roEvtCmd   );
    void  OnSelSimEng( wxCommandEvent & roEvtCmd   );
    void  OnPrefs    ( wxCommandEvent & roEvtCmd   );
    void  OnManUser  ( wxCommandEvent & roEvtCmd   );
    void  OnManNGSP  ( wxCommandEvent & roEvtCmd   ) { } // Not yet implemented ???
    void  OnManGCAP  ( wxCommandEvent & roEvtCmd   ) { } // Not yet implemented ???
    void  OnAbout    ( wxCommandEvent & roEvtCmd   );
    void  OnToolEnter( wxCommandEvent & roEvtCmd   );
    void  OnSysExit  ( wxCloseEvent   & roEvtClose );

    // Declare friend classes
    friend  class  FileTasks;

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum eFrmItemID
    {
      ID_MNU_OPEN = 1,
      ID_MNU_IMPORT,
      ID_MNU_RELOAD,
      ID_MNU_CLOSE,
      ID_MNU_QUIT,
      ID_MNU_CREATE,
      ID_MNU_RUN,
      ID_MNU_STOP,
      ID_MNU_SCHEM,
      ID_MNU_VIEWER,
      ID_MNU_NGSPICE,
      ID_MNU_GNUCAP,
      ID_MNU_PREFS,
      ID_MNU_MAN_USER,
      ID_MNU_MAN_NGSP,
      ID_MNU_MAN_GCAP,
      ID_MNU_ABOUT,

      ID_TBR_OPEN,
      ID_TBR_IMPORT,
      ID_TBR_RELOAD,
      ID_TBR_CLOSE,
      ID_TBR_CREATE,
      ID_TBR_RUN,
      ID_TBR_STOP,
      ID_TBR_SCHEM,
      ID_TBR_VIEWER,
      ID_TBR_HELP,
      ID_TBR_FST = ID_TBR_OPEN,
      ID_TBR_LST = ID_TBR_HELP,

      ID_LBX_NODES,
      ID_LBX_CPNTS,

      ID_NBK_ANALYSIS,
      ID_NBK_TXTCTRLS,

      ID_UNUSED,      // Assigned to controls for which events are not used
    };

    // Leave this as the last line as private access is envoked by this macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // FRMMAIN_HPP

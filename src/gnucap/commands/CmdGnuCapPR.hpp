//**************************************************************************************************
//                                         CmdGnuCapPR.hpp                                         *
//                                        -----------------                                        *
// Description : This class contains values associated with the GNU-Cap PRINT command. It inherits *
//               from the class CmdBase.                                                           *
// Started     : 2008-03-17                                                                        *
// Last Update : 2018-09-23                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDGCPPR_HPP
#define CMDGCPPR_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "netlist/Component.hpp"
#include "netlist/NetList.hpp"
#include "ngspice/commands/CmdNgSpicePR.hpp"

class CmdNgSpicePR;

//**************************************************************************************************

class CmdGnuCapPR : public CmdBase
{
  private :

    eTypeCmd  m_eAnaType;

    bool  bValidate( void ) override;

  public :

    wxArrayString  m_osaNodes;
    wxArrayString  m_osaCpnts;
    bool           m_bParams[ ePARAM_NONE ];
    bool           m_bCpxPts[ eCPXPT_NONE ];

          CmdGnuCapPR( void );
         ~CmdGnuCapPR( );

    bool  bSetDefaults( void ) override;
    bool  bSetAnaType ( eTypeCmd eAnaType );

    eTypeCmd  eGetAnaType( void ) const { return( m_eAnaType ); }

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    CmdGnuCapPR & operator = ( const CmdNgSpicePR & roCmdPR );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDGCPPR_HPP

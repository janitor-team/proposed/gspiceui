//**************************************************************************************************
//                                          PrcGSchem.hpp                                          *
//                                         ---------------                                         *
// Description : Provides an interface to the application gschem which is a schematic editor. If   *
//               the currently loaded netlist file is derived from a gSchem schematic file the     *
//               user can envoke gschem and edit the schematic.                                    *
// Started     : 2008-02-18                                                                        *
// Last Update : 2018-10-10                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PRCGSCHEM_HPP
#define PRCGSCHEM_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "base/PrcBase.hpp"

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class PrcGSchem : public PrcBase
{
  private :

    ArrayFileName  m_oaNameSchems;  // The full names of the schematic file/s

  public :

          PrcGSchem( void );
         ~PrcGSchem( );

    bool  bSetEdaSuite  (       eTypeEDA           eEDA       );
    bool  bSetSchemFiles( const wxString      &  rosFileNames );
    bool  bSetSchemFiles( const wxArrayString & roasFileNames );

          eTypeEDA          eGetEdaSuite  ( void );
    const wxString      & rosGetSchemFiles( void );
    const ArrayFileName & roaGetSchemFiles( void ) { return( m_oaNameSchems ); }

    virtual  bool  bExec( void );
};

//**************************************************************************************************

#endif // PRCGSCHEM_HPP

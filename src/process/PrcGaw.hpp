//**************************************************************************************************
//                                           PrcGaw.hpp                                            *
//                                          ------------                                           *
// Description : Provides an interface to the utility gaw which is a waveform viewer. The user can *
//               view the raw simulation data or envoke this utility which plots the data          *
//               graphically.                                                                      *
// Started     : 2008-01-10                                                                        *
// Last Update : 2016-03-25                                                                        *
// Copyright   : (C) 2008-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PRCGAW_HPP
#define PRCGAW_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PrcBase.hpp"

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class PrcGaw : public PrcBase
{
  private :

    wxFileName  m_oNameResults;  // The full simulator results file name

    // Functions for reformatting the results file for input to the viewer
    bool  bFilterFile( void );

  public :

          PrcGaw( void );
         ~PrcGaw( );

    bool  bSetResultsFile( const wxString & rosFileName );

    const wxFileName & roGetResultsFile( void ) { return( m_oNameResults ); }

    virtual  bool  bExec( void );
};

//**************************************************************************************************

#endif // PRCGAW_HPP

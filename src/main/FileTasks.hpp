//**************************************************************************************************
//                                         FileTasks.hpp                                           *
//                                        ---------------                                          *
// Description : This is a helper class for FrmMain, it handles most of the file operations.       *
// Started     : 2005-05-28                                                                        *
// Last Update : 2018-10-28                                                                        *
// Copyright   : (C) 2005-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef FILETASKS_HPP
#define FILETASKS_HPP

// Application includes

class FrmMain;

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "base/SimnBase.hpp"
#include "process/PrcGSchem.hpp"
#include "process/PrcGNetList.hpp"

// wxWidgets includes

#include <wx/config.h>

//**************************************************************************************************

class FileTasks
{
  private :

    FrmMain    * m_poFrmMain;      // Pointer to application main frame
    PrcGNetList  m_oPrc_gnetlist;  // Netlist file conversion utility

    // Object initialization functions
    void  InitGuileProc ( void );
    void  InitSchemFiles( void );
    void  InitNetLstFile( void );

    void  DlgErrSchems  ( void );
    void  DlgErrNetLst  ( void );

    bool  bExecImport   ( void );

  public :

    explicit  FileTasks( FrmMain * poFrmMain );
             ~FileTasks( );

    void  Initialize( void );  // This function calls all the initialization fns

    bool  bIsOk_gnetlist( void );

    bool  bSetTitle     ( void );
    bool  bSetGuileProc ( const wxString      &  rosProcName  );
    bool  bSetSchemFiles( const wxString      & roasFileNames );
    bool  bSetSchemFiles( const wxArrayString & roasFileNames );
    bool  bSetNetLstFile( const wxString      &  rosFileName=GNETLST_USE_SCHEM );
    bool  bSetLogFile   ( const wxString      &  rosFileName  )
                                           { return( m_oPrc_gnetlist.bSetLogFile( rosFileName ) ); }

    const wxString      &  rosGetGuileProc ( void );
    const wxArrayString & rosaGetSchemFiles( void );
    const wxString      &  rosGetNetLstFile( void );
    const wxString      &  rosGetLogFile   ( void );

    bool  bDlgOpen    ( void );
    bool  bDlgImport  ( void );
    bool  bDelTmpFiles( void );

    bool  bOpen  ( void );
    bool  bImport( void );
    bool  bReload( void );
    bool  bClose ( void );
    bool  bExit  ( void );

    bool  bReloadReqd( void );

    // Declare friend classes
    friend  class  FrmMain;
};

//**************************************************************************************************

#endif // FILETASKS_HPP

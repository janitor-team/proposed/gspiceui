//**************************************************************************************************
//                                           CmdBase.hpp                                           *
//                                          -------------                                          *
// Description : This is the base class for all command classes. It has some characteristics of    *
//               structure in that some attributes have public access; this simplifies the class   *
//               interface.                                                                        *
// Started     : 2006-08-31                                                                        *
// Last Update : 2018-09-25                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDBASE_HPP
#define CMDBASE_HPP

// Application Includes

#include "TypeDefs.hpp"

//**************************************************************************************************

class CmdBase : public wxString
{
  protected :

    eTypeSimEng  m_eSimEng;
    eTypeCmd     m_eCmdType;

    wxString     m_osErrMsg;

    virtual  bool  bValidate( void );

  public :

                   CmdBase( void );
                   CmdBase( const CmdBase & roCmd );
                  ~CmdBase( );

    virtual  bool  bSetDefaults( void );
             bool  bIsValid    ( void )             { return( m_osErrMsg.IsEmpty( ) ); }

    virtual  bool  bParse ( void ) = 0;
    virtual  bool  bFormat( void ) = 0;

             bool  bSetString( wxString & ros );

                   eTypeSimEng  eGetSimEng ( void ) { return( m_eSimEng  ); }
                   eTypeCmd     eGetCmdType( void ) { return( m_eCmdType ); }
             const wxString & rosGetErrMsg ( void ) { return( m_osErrMsg ); }

             void  SetErrMsg( const wxString & rosErrMsg )
                                                    { if( bIsValid( ) ) m_osErrMsg = rosErrMsg; }

    virtual  CmdBase & operator = ( const CmdBase & roCmd );

    virtual  void  Print( const wxString & rosPrefix=wxT("") );
};

//**************************************************************************************************

#endif // CMDBASE_HPP

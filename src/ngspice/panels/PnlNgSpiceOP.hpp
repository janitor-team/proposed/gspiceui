//**************************************************************************************************
//                                        PnlNgSpiceOP.hpp                                         *
//                                       ------------------                                        *
// Description : This class derives from the PnlAnaBase base class and provides a GUI for the user *
//               to configure an Operating Point Analysis for the NG-Spice electronic circuit      *
//               simulation engine.                                                                *
// Started     : 2015-01-12                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2015-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLNGSPICEOP_HPP
#define PNLNGSPICEOP_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PnlAnaBase.hpp"
#include "netlist/SimnNgSpice.hpp"

//**************************************************************************************************

class PnlNgSpiceOP : public PnlAnaBase
{
  private :

    void  Create      ( void );
    void  CreateSigSrc( void ) override;

  public :

    explicit  PnlNgSpiceOP( wxWindow * poWin );
             ~PnlNgSpiceOP( );

    bool  bClear( void ) override;

    bool  bLoad( SimnNgSpice & roSimn );
    bool  bSave( SimnNgSpice & roSimn );

    // Event handlers
    void  OnSrcName( wxCommandEvent & roEvtCmd );

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // PNLNGSPICEOP_HPP

//**************************************************************************************************
//                                         PrcGNetList.hpp                                         *
//                                        -----------------                                        *
// Description : Provides a programatic interface to the utility gnetlist which is used to         *
//               generate netlist file from various schematic editor formats.                      *
// Started     : 2004-01-29                                                                        *
// Last Update : 2018-10-28                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PRCGNETLIST_HPP
#define PRCGNETLIST_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "base/PrcBase.hpp"

// wxWidgets Includes

#include <wx/filename.h>
#include <wx/regex.h>
#include <wx/tokenzr.h>

// Local Constant Declarations

#define  GNETLST_LOG_FILE    wxT("gspiceui.log")    // Default log file name
#define  GNETLST_GUILE_PROC  wxT("spice-sdb")       // Default Guile procedure
#define  GNETLST_USE_SCHEM   wxT("use-schem-name")  // Use schematic name for to create netlist name
#define  GNETLST_CUR_PROC    1000                   // A value greater than the no. of Guile proc's

//**************************************************************************************************

class PrcGNetList : public PrcBase
{
  private :

            size_t         m_szGuileProc;    // The Guile procedure offset into m_oasGuileProcs[ ]
    static  wxArrayString  m_oasGuileProcs;  // Array of all possible Guile procedures
            ArrayFileName  m_oaNameSchems;   // The full names of the schematic file/s
            wxFileName     m_oNameNetList;   // The full name  of the list file

    void  InitGuileProcs( void );

  public :

          PrcGNetList( void );
         ~PrcGNetList( );

    bool  bClear( void );

    bool  bSetEdaSuite  (       eTypeEDA           eEDA );
    bool  bSetGuileProc ( const wxString      &  rosGProc );
    bool  bSetSchemFiles( const wxString      &  rosFileNames );
    bool  bSetSchemFiles( const wxArrayString & roasFileNames );
    bool  bSetNetLstFile( const wxString      &  rosFileName=GNETLST_USE_SCHEM );

                 eTypeEDA           eGetEdaSuite  ( void );
    static const wxArrayString & roasGetGuileProcs( void ) { return( m_oasGuileProcs ); }
           const wxString      &  rosGetGuileProc ( size_t szIndex=GNETLST_CUR_PROC );
           const ArrayFileName &  roaGetSchemFiles( void ) { return( m_oaNameSchems  ); }
           const wxFileName    &   roGetSchemFile ( size_t szIndex=0 );
           const wxFileName    &   roGetNetLstFile( void ) { return( m_oNameNetList  ); }

    virtual  bool  bExec( void );

    // The following function is intended for debugging
    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // PRCGNETLIST_HPP

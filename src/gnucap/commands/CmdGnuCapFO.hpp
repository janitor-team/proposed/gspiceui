//**************************************************************************************************
//                                         CmdGnuCapFO.hpp                                         *
//                                        -----------------                                        *
// Description : This class contains values associated with the GNU-Cap FOURIER command. It        *
//               inherits from the class CmdBase.                                                  *
// Started     : 2008-03-11                                                                        *
// Last Update : 2018-09-23                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDGCPFO_HPP
#define CMDGCPFO_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "utility/CnvtType.hpp"

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class CmdGnuCapFO : public CmdBase
{
  private :

    bool  bValidate( void ) override;

  public :

    wxString    m_osStart;  // The sweep start value
    wxString    m_osStop;   // The sweep stop  value
    wxString    m_osStep;   // The sweep step  value
    eTypeInitC  m_eInitC;   // The sweep scale type
    wxString    m_osTempC;  // The analysis temperature

          CmdGnuCapFO( void );
         ~CmdGnuCapFO( );

    bool  bSetDefaults( void ) override;

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDGCPFO_HPP

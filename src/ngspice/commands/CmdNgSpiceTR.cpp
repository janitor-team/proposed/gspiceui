//**************************************************************************************************
//                                        CmdNgSpiceTR.cpp                                         *
//                                       ------------------                                        *
// Started     : 2007-10-15                                                                        *
// Last Update : 2018-09-25                                                                        *
// Copyright   : (C) 2007-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "CmdNgSpiceTR.hpp"

//**************************************************************************************************
// Constructor.

CmdNgSpiceTR::CmdNgSpiceTR( void ) : CmdBase( )
{
  bSetDefaults( );
}

//**************************************************************************************************
// Destructor.

CmdNgSpiceTR::~CmdNgSpiceTR( )
{
}

//**************************************************************************************************
// Check that the object attributes are valid.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdNgSpiceTR::bValidate( void )
{
  double  df1, df2, df3;

  CmdBase::bValidate( );

  // Check the sweep start and stop values
  if( ! CnvtType::bStrToFlt( m_osStart, &df1 ) ) SetErrMsg( wxT("Invalid sweep start value.") );
  if( ! CnvtType::bStrToFlt( m_osStop,  &df2 ) ) SetErrMsg( wxT("Invalid sweep stop value.") );
  if( df2 < df1 )                                SetErrMsg( wxT("Start value greater than stop value.") );

  // Check the sweep step value and initial conditions
  if( ! CnvtType::bStrToFlt( m_osStep, &df3 ) )  SetErrMsg( wxT("Invalid sweep step value.") );
  if( df3 <= 0.0 )                               SetErrMsg( wxT("Step value less than or equal to zero.") );
  switch( m_eInitC )
  {
    case eINITC_WARM :
    case eINITC_UICS :
    case eINITC_COLD : break;
    default          :                           SetErrMsg( wxT("Invalid initial conditions value.") );
  }

  return( bIsValid( ) );
}

//**************************************************************************************************
// Set the object attributes to they're default values.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdNgSpiceTR::bSetDefaults( void )
{
  CmdBase::bSetDefaults( );

  m_eSimEng  = eSIMR_NGSPICE;
  m_eCmdType = eCMD_TR;

  m_osStart = wxT("0.0m");
  m_osStop  = wxT("100.0m");
  m_osStep  = wxT("10.0m");

  m_eInitC = eINITC_WARM;

  return( true );
}

//**************************************************************************************************
// Parse the command string.
//
// Eg.s : .TRAN 10.00m 100.00m 0.00 10.00m UIC
//        .TRAN 10.00m 100.00m 0.00 10.00m
//        .TRAN 10.00m 100.00m 0.00
//        .TRAN 10.00m 100.00m
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdNgSpiceTR::bParse( void )
{
  wxStringTokenizer  ostk1;
  wxString           os1;
  double             df1;
  int                i1;

  // Clear the object attributes
  os1 = *this;
  bSetDefaults( );
  assign( os1 );

  // Tokenize the command string
  ostk1.SetString( *this );
  i1 = ostk1.CountTokens( );
  if( i1<3 || i1>6 )      return( bValidate( ) );

  // Check command type
  os1 = ostk1.GetNextToken( ).Left( 3 ).Upper( );
  if( os1 != wxT(".TR") ) return( bValidate( ) );

  // Extract the step increment (must be present)
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToFlt( os1, &df1 ) )   m_osStep  = os1;
  else                    return( bValidate( ) );

  // Extract the stop value (must be present)
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToFlt( os1, &df1 ) )   m_osStop  = os1;
  else                    return( bValidate( ) );

  // Extract the start value (may be omitted)
  m_osStart = wxT("0.0");
  if( ostk1.HasMoreTokens( ) )
  {
    os1 = ostk1.GetNextToken( );
    if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osStart = os1;
    else                  return( bValidate( ) );
  }

  // Extract the initial conditions specifier
  m_eInitC = eINITC_WARM;
  while( ostk1.HasMoreTokens( ) )
  {
    os1 = ostk1.GetNextToken( ).Upper( );
    if( os1 == wxT("UIC") )
    {
      m_eInitC = eINITC_UICS;
      break;
    }
  }

  return( bValidate( ) );
}

//**************************************************************************************************
// Format the command string.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdNgSpiceTR::bFormat( void )
{
  wxString  osCmd;

  // Set the command name
  osCmd = wxT(".TRAN");

  // Set sweep parameters
  osCmd << wxT(' ') << m_osStep;
  osCmd << wxT(' ') << m_osStop;
  osCmd << wxT(' ') << m_osStart;

  // Set initial conditions
  if( m_eInitC != eINITC_WARM ) osCmd << wxT(" UIC");

  assign( osCmd );

  return( bValidate( ) );
}

//**************************************************************************************************
// Copy the contents of a CmdGnuCapAC object.
//
// Argument List :
//   roCmdTR - A reference to a CmdGnuCapTR object
//
// Return Values :
//   A reference to this object

CmdNgSpiceTR & CmdNgSpiceTR::operator = ( const CmdGnuCapTR & roCmdTR )
{
  (CmdBase &) *this = (CmdBase &) roCmdTR;

  m_osStart = roCmdTR.m_osStart;
  m_osStop  = roCmdTR.m_osStop;
  m_osStep  = roCmdTR.m_osStep;
  m_eInitC  = roCmdTR.m_eInitC;

  bFormat( );

  return( *this );
}

//**************************************************************************************************
// Print the object attributes.
//
// Argument List :
//   rosPrefix - A prefix to every line displayed (usually just spaces)

void  CmdNgSpiceTR::Print( const wxString & rosPrefix )
{
  CmdBase::Print( rosPrefix + wxT("CmdBase::") );

  std::cout << rosPrefix.mb_str( ) << "m_osStart : " << m_osStart.mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osStop  : " << m_osStop .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osStep  : " << m_osStep .mb_str( ) << '\n';

  std::cout << rosPrefix.mb_str( ) << "m_eInitC  : ";
  switch( m_eInitC )
  {
    case eINITC_WARM : std::cout << "eINITC_COLD\n"; break;
    case eINITC_UICS : std::cout << "eINITC_UICS\n"; break;
    case eINITC_COLD : std::cout << "eINITC_COLD\n"; break;
    case eINITC_NONE : std::cout << "eINITC_NONE\n"; break;
    default :          std::cout << "Invalid\n";     break;
  }
}

//**************************************************************************************************
//                                          Test Utility                                           *
//**************************************************************************************************

#ifdef TEST_CMDNGSPICETR

using  namespace  std;

// Function prototypes

void  Usage( char * psAppName );

//**************************************************************************************************

int  main( int argc, char * argv[ ] )
{
  wxString  osCmd;
  wxString  os1;

  // Validate the argument count passed to the application
  if( argc > 2 )           { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }

  // Process the command line arguments
  os1 = wxConvLibc.cMB2WC( argv[ 1 ] );
  if( argc > 1 )
  {
    if( os1 == wxT("-h") ) { Usage( argv[ 0 ] ); exit( EXIT_SUCCESS ); }
    else                   { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }
  }

  // Display the utility banner
  cout << "\n  Class CmdNgSpiceTR Test Utility"
       << "\n     Version 1.03 (2016-09-29)\n";

  // Create a NG-SPICE TRANSIENT command object
  CmdNgSpiceTR  oCmd_TR;

  // Use the following command example to check the formatter and the parser :
  osCmd = wxT(".TRAN 10.00m 100.00m 0.00 UIC");

  // Set things up for a formatter test
  oCmd_TR.m_osStart = wxT("0.00");
  oCmd_TR.m_osStop  = wxT("100.00m");
  oCmd_TR.m_osStep  = wxT("10.00m");
  oCmd_TR.m_eInitC = eINITC_UICS;
  cout << "\nRun Formatter    : " << ( oCmd_TR.bFormat( ) ? "OK" : "FAULT" );
  cout << "\nTest Cmd Format  : " << ( oCmd_TR == osCmd   ? "OK" : "FAULT" );
  cout << "\nExample Command  : " << osCmd  .mb_str( );
  cout << "\noCmd_TR Contents : " << oCmd_TR.mb_str( ) << '\n';

  // Set things up for a parser test
  oCmd_TR.bSetString( osCmd );
  cout << "\nRun Parser       : " << ( oCmd_TR.bParse( ) ? "OK" : "FAULT" );
  oCmd_TR.bFormat( );
  cout << "\nTest Cmd Format  : " << ( oCmd_TR == osCmd  ? "OK" : "FAULT" );
  cout << "\nExample Command  : " << osCmd  .mb_str( );
  cout << "\noCmd_TR Contents : " << oCmd_TR.mb_str( ) << '\n';

  cout << '\n';

  exit( EXIT_SUCCESS );
}

//**************************************************************************************************

void  Usage( char * psAppName )
{
  cout << "\nUsage   : " << psAppName << " [-OPTIONS]"
       << "\nOptions :"
       << "\n  -h : Print usage (this message)\n";
}

#endif // TEST_CMDNGSPICETR

//**************************************************************************************************

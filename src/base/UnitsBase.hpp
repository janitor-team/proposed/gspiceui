//**************************************************************************************************
//                                          UnitsBase.hpp                                          *
//                                         ---------------                                         *
//                                                                                                 *
// Description : This is the base class for controls which display units.                          *
// Started     : 2014-03-08                                                                        *
// Last Update : 2015-01-18                                                                        *
// Copyright   : (C) 2014-2015 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef UNITSBASE_HPP
#define UNITSBASE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "utility/CnvtType.hpp"

//**************************************************************************************************

class UnitsBase
{
  protected :

                   UnitsBase( void );
                  ~UnitsBase( );

  public :

    virtual  bool  bIsCreated( void ) = 0;

    virtual  const wxString & rosGetUnits( void ) = 0;
    virtual        int          iGetUnits( void ) = 0;

    // Take a value and return it combined with the currently selected units
          long        liGetValue( double dfValue );
          double      dfGetValue( double dfValue );
    const wxString & rosGetValue( double dfValue, int iRes=2 );

    // Static helper functions (may be called without instantiating the class)
    static  bool  bGetUnitsType( eTypeUnits eUType, wxString & rosUnits );
    static  bool  bGetUnitsType( const wxString & rosUnits, eTypeUnits * peUType );
};

//**************************************************************************************************

#endif // UNITSBASE_HPP

//**************************************************************************************************
//                                        CmdNgSpiceOPT.hpp                                        *
//                                       -------------------                                       *
// Description : This class contains values associated with the NG-Spice OPTIONS command. It       *
//               inherits from the class CmdBase.                                                  *
// Started     : 2006-08-23                                                                        *
// Last Update : 2018-11-04                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDNGSOPT_HPP
#define CMDNGSOPT_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "gnucap/commands/CmdGnuCapOPT.hpp"
#include "utility/CnvtType.hpp"

class CmdGnuCapOPT;

// Local Macro and/or Constant Declarations

#define  NGS_ABSTOL    wxT("1.0pA")      // Absolute current error tolerance
#define  NGS_CHGTOL    wxT("10.0fC")     // Charge tolerance
#define  NGS_PIVTOL    wxT("1.0E-13")    // Absolute minimum value for matrix entry as pivot
#define  NGS_RELTOL    wxT("0.001")      // Relative error tolerance
#define  NGS_TRTOL     wxT("7.0")        // Transient analysis error tolerance
#define  NGS_VNTOL     wxT("1.0uV")      // Absolute voltage error tolerance
#define  NGS_PIVREL    wxT("1.0E-03")    // Relative ratio of largest col. entry & acceptable pivot value
#define  NGS_DEFL      wxT("100.0um")    // MOSFET default channel length (metre)
#define  NGS_DEFW      wxT("100.0um")    // MOSFET default channel width  (metre)
#define  NGS_DEFAD     wxT("0.0")        // MOSFET default drain  diffusion area (sq.m)
#define  NGS_DEFAS     wxT("0.0")        // MOSFET default source diffusion area (sq.m)
#define  NGS_TEMP      wxT("27.0")       // Operating temperature of circuit
#define  NGS_TNOM      wxT("27.0")       // Nominal temp. device parameters measured at
#define  NGS_CONVABSS  wxT("0.1")        // Absolute step limit applied to code models
#define  NGS_CONVSTEP  wxT("0.25")       // Relative step limit applied to code models
#define  NGS_GMIN      wxT("1.0pS")      // Minimum allowable conductance
#define  NGS_RSERIES   wxT("0.0mOhm")    // Adds a resistor to each inductor in the circuit
#define  NGS_RSHUNT    wxT("1000.0GOhm") // Adds a resistor from each analogue node to ground
#define  NGS_RAMPTIME  wxT("0.0mSec")    // Transient analysis supply ramping time

#define  NGS_ITL1      wxT("100")        // DC analysis iteration limit
#define  NGS_ITL2      wxT("50")         // DC analysis transfer curve iteration limit
#define  NGS_ITL3      wxT("4")          // Transient analysis lower iteration limit
#define  NGS_ITL4      wxT("10")         // Transient analysis timepoint iteration limit
#define  NGS_ITL5      wxT("5000")       // Transient analysis total iteration limit
#define  NGS_ITL6      wxT("10")         // Source stepping iteration limit (synonym for SRCSTEPS)
#define  NGS_SRCSTEPS  wxT("10")         // Source stepping iteration limit
#define  NGS_GMINSTEP  wxT("0")          // Number of GMIN steps to be attempted
#define  NGS_MAXEVIT   wxT("5")          // Max. event iterations at an analysis point
#define  NGS_MAXOPALT  wxT("5")          // Max. analog/event alternations in OP analysis
#define  NGS_MAXORD    wxT("2")          // Max. order for the numerical integration method
#define  NGS_MAXWARNS  wxT("5")          // Max. no. of SOA (Safe Operating Area) voltage warning messages
#define  NGS_NUMDGT    wxT("6")          // Precision : no. of digits after decimal point
#define  NGS_WIDTH     wxT("0")          // Allows for operator (eg. frequency) & 10 cols of data

#define  NGS_METHOD    wxT("Trap")       // Numerical integration method (TRAPezoidal or GEAR)
#define  NGS_UNITS     wxT("Degrees")    // Phase angle units : degrees or radians

#define  NGS_BADMOS3   false             // Use MOS3 model with "kappa" discontinuity
#define  NGS_INTERP    false             // Interpolate output data onto fixed time steps
#define  NGS_NOOPAC    false             // Don't do an OP analysis before the AC analysis
#define  NGS_NOOPALT   false             // Analog/event alternation in OP analysis
#define  NGS_OPTS      false             // Enable printing of option values
#define  NGS_TRYTOCOM  false             // Try to condense LTRA model's past history
#define  NGS_WARN      false             // Enable SOA (Safe Operating Area) voltage warning messages

//**************************************************************************************************

class CmdNgSpiceOPT : public CmdBase
{
  private :

    bool  bValidate( void ) override;

  public :

    // Options which are floating point values
    wxString  m_osABSTOL;
    wxString  m_osCHGTOL;
    wxString  m_osCONVABSS;
    wxString  m_osCONVSTEP;
    wxString  m_osDEFL;
    wxString  m_osDEFW;
    wxString  m_osDEFAD;
    wxString  m_osDEFAS;
    wxString  m_osGMIN;
    wxString  m_osPIVREL;
    wxString  m_osPIVTOL;
    wxString  m_osRAMPTIME;
    wxString  m_osRELTOL;
    wxString  m_osRSERIES;
    wxString  m_osRSHUNT;
    wxString  m_osTEMP;
    wxString  m_osTNOM;
    wxString  m_osTRTOL;
    wxString  m_osVNTOL;

    // Options which are integer values
    wxString  m_osGMINSTEP;
    wxString  m_osITL1;
    wxString  m_osITL2;
    wxString  m_osITL3;
    wxString  m_osITL4;
    wxString  m_osITL5;
    wxString  m_osITL6;
    wxString  m_osMAXEVIT;
    wxString  m_osMAXOPALT;
    wxString  m_osMAXORD;
    wxString  m_osMAXWARNS;
    wxString  m_osNUMDGT;
    wxString  m_osSRCSTEPS;
    wxString  m_osWIDTH;

    // Options which are string values
    wxString  m_osMETHOD;
    wxString  m_osUNITS;

    // Options which are boolean values
    bool      m_bBADMOS3;
    bool      m_bINTERP;
    bool      m_bNOOPAC;
    bool      m_bNOOPALT;
    bool      m_bOPTS;
    bool      m_bTRYTOCOM;
    bool      m_bWARN;

          CmdNgSpiceOPT( void );
         ~CmdNgSpiceOPT( );

    bool  bSetDefaults( void ) override;

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    CmdNgSpiceOPT & operator = ( const CmdGnuCapOPT & roCmdOPT );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDNGSOPT_HPP

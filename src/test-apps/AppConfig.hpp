//**************************************************************************************************
//                                         AppConfig.hpp                                           *
//                                        ---------------                                          *
// Description : This is a wxWidgets application designed to test and debug the Config class. This *
//               application also demonstrates how to implement a console only app. in wxWidgets.  *
// Started     : 2016-09-27                                                                        *
// Last Update : 2016-09-27                                                                        *
// Copyright   : (C) 2016 MSWaters                                                                 *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

// wxWidgets includes

#include <wx/cmdline.h>

// Application Includes

#include "Config.hpp"

//**************************************************************************************************

class AppConfig : public wxApp
{
    Config  m_oConfig;

  public :

    void  Usage( void );

    // Event handlers
    virtual  bool  OnInit  ( void );
//  virtual  int   OnRun   ( void );
    virtual  int   MainLoop( void );
//  virtual  int   OnExit  ( void );
};

//**************************************************************************************************

//**************************************************************************************************
//                                          NbkGnuCap.hpp                                          *
//                                         ---------------                                         *
// Description : Provides a container class for the analysis configuration panels for the          *
//               different analyses provided by GNU-Cap electronic circuit simulator.              *
// Started     : 2003-09-05                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2003-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef NBKGNUCAP_HPP
#define NBKGNUCAP_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "base/NbkSimEngBase.hpp"
#include "gnucap/dialogs/DlgGnuCapCfgOPT.hpp"
#include "gnucap/panels/PnlGnuCapOP.hpp"
#include "gnucap/panels/PnlGnuCapDC.hpp"
#include "gnucap/panels/PnlGnuCapAC.hpp"
#include "gnucap/panels/PnlGnuCapTR.hpp"
#include "gnucap/panels/PnlGnuCapFO.hpp"

//**************************************************************************************************

class NbkGnuCap : public NbkSimEngBase
{
  private:

    // Analysis pages
    PnlGnuCapOP * m_poPnlGnuCapOP;
    PnlGnuCapDC * m_poPnlGnuCapDC;
    PnlGnuCapAC * m_poPnlGnuCapAC;
    PnlGnuCapTR * m_poPnlGnuCapTR;
//    PnlGnuCapFO * m_poPnlGnuCapFO;

    // OPTIONS line setup dialog
    DlgGnuCapCfgOPT  m_oDlgCfgOPT;

  public:

          NbkGnuCap( wxWindow * poParent, wxWindowID oWinID );
         ~NbkGnuCap( );

    bool  bClear( void ) override;

    bool  bLoad( const SimnBase & roSimn ) override;
    bool  bSave(       SimnBase & roSimn ) override;

    bool  bSetPage(       eTypeCmd     eAnalysis ) override;
    bool  bSetPage( const wxString & rosAnalysis ) override;

    const  wxString & rosGetPage( void ) override;
           eTypeCmd     eGetPage( void ) override;

    // Event handlers
    void  OnPageChangd( wxNotebookEvent & roEvtNbk );
    void  OnBtnOptions( wxCommandEvent  & roEvtCmd );

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // NBKGNUCAP_HPP

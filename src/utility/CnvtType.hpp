//**************************************************************************************************
//                                          CnvtType.hpp                                           *
//                                         --------------                                          *
// Description : This class is intended to provide useful functionality for converting basic types *
//               one to another eg. wxString to long. The strings may contain alpha scaling        *
//               factors eg. K for Kilo.                                                           *
// Started     : 2004-09-21                                                                        *
// Last Update : 2016-09-03                                                                        *
// Copyright   : (C) 2004-2016 MSWaters                                                            *
// Note        : This class cannot be instanciated. It is intended as a container for related      *
//               functionality. A static member function of a class may be accessed without using  *
//               an object of that class. A static member function does not have a 'this' pointer. *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CNVTTYPE_HPP
#define CNVTTYPE_HPP

// System Includes

#include <cmath>

#if !defined( __WXGTK__ ) && !defined( __WXMSW__ ) && !defined( __WXOSX__ )
  #include <string.h>
#endif

// Application includes

#include "TypeDefs.hpp"

// Macro declarations to convert between degree Celsius and Fahrenheit.

#define  TEMP_DEGF2C(X)  ((X - 32.0) * 100.0 / (212.0 - 32.0))  // Convert from Fahrenheit to Deg.C
#define  TEMP_DEGC2F(X)  (X * ((212.0 - 32.0) / 100.0) + 32.0)  // Convert from Celsius    to Deg.F

// Local Constant Declarations

#if defined( __WXGTK__ ) || defined( __WXMSW__ ) || defined( __WXOSX__ )
  #define  TYPE_STR                wxString
  #define  TYPE_CHR                wxChar
  #define  C_STR                   mb_str
  #define  IS_DIGIT                wxIsdigit
#else
  #define  TYPE_STR                std::string
  #define  TYPE_CHR                char
  #define  C_STR                   c_str
  #define  IS_DIGIT                isdigit
  #define  wxT(X)                  X
  #define  wxCHECK_VERSION(X,Y,Z)  1
#endif

#define  CNVT_DEF_FLT_RES   3  // Default floating point number resolution
#define  CNVT_MIN_FLT_RES   1  // Minimum floating point number resolution
#define  CNVT_MAX_FLT_RES  10  // Maximum floating point number resolution

//**************************************************************************************************

class CnvtType
{
  private :

    static  int   m_iFltRes;

                  CnvtType( void );
                 ~CnvtType( );

#ifdef TEST_CNVTTYPE
  public :
#endif

    static  TYPE_STR & rosGetNum( const TYPE_STR & rosNum );

#ifndef TEST_CNVTTYPE
  public :
#endif

    static  bool  bIsEqual( float   f1, float   f2 );
    static  bool  bIsEqual( double df1, double df2 );

    static  int   iGetFltRes   ( void )             { return( m_iFltRes ); }
    static  bool  bSetFltRes   ( int iRes );

    static  bool  bIsInteger   ( const TYPE_STR & rosStr );
    static  bool  bIsFloat     ( const TYPE_STR & rosStr );

    static  bool  bParseFlt    ( float   fNum, float  *  pfMan, int * piExp );
    static  bool  bParseFlt    ( double dfNum, double * pdfMan, int * piExp );
    static  bool  bParseDbl    ( double dfNum, double * pdfMan, int * piExp )
                                                    { return( bParseFlt( dfNum, pdfMan, piExp ) ); }

    static  bool  bStrToInt    ( const TYPE_STR & rosNum, int  *  piNum );
    static  bool  bStrToInt    ( const TYPE_STR & rosNum, long * pliNum );
    static  bool  bStrToLong   ( const TYPE_STR & rosNum, long * pliNum )
                                                    { return( bStrToInt( rosNum, pliNum ) ); }

    static  bool  bStrToFlt    ( const TYPE_STR & rosNum, float  *  pfNum );
    static  bool  bStrToFlt    ( const TYPE_STR & rosNum, double * pdfNum );
    static  bool  bStrToDbl    ( const TYPE_STR & rosNum, double * pdfNum )
                                                    { return( bStrToFlt( rosNum, pdfNum ) ); }

    static  bool  bIntToStr    ( int    iNum , TYPE_STR & rosNum );
    static  bool  bIntToStr    ( long  liNum , TYPE_STR & rosNum );
    static  bool  bLongToStr   ( long  liNum , TYPE_STR & rosNum )
                                                    { return( bIntToStr( liNum, rosNum ) ); }

    static  bool  bFltToStr    ( float  fNum , TYPE_STR & rosNum );
    static  bool  bFltToStr    ( double dfNum, TYPE_STR & rosNum );
    static  bool  bDblToStr    ( double dfNum, TYPE_STR & rosNum )
                                                    { return( bFltToStr( dfNum, rosNum ) ); }

    static  bool  bFltToStrEng ( float  fNum , TYPE_STR & rosNum );
    static  bool  bFltToStrEng ( double dfNum, TYPE_STR & rosNum );
    static  bool  bDblToStrEng ( double dfNum, TYPE_STR & rosNum )
                                                    { return( bFltToStrEng( dfNum, rosNum ) ); }

    static  bool  bUnitPfxToExp( TYPE_CHR ocPfx, int * piExp );
    static  bool  bUnitExpToPfx( int iExp, TYPE_CHR * pocPfx );
};

//**************************************************************************************************

#endif // CNVTTYPE_HPP

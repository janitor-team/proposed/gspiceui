v 20110115 2
C 40 40 0 0 0 title-A4.sym
C 5900 4900 1 270 0 resistor-2.sym
{
T 6200 4500 5 10 1 1 0 0 1
refdes=R1
T 6200 4300 5 10 1 1 0 0 1
value=2.7
}
T 5300 800 9 14 1 0 0 0 2
PowerTip  PG24064  Graphical  LCD  Display
           LED  Backlight  Level  Control 
C 10600 3400 1 0 1 vdc-1.sym
{
T 9800 4250 5 10 0 1 0 6 1
refdes=Vcc
T 9900 3900 5 10 1 1 0 6 1
value=DC 5V
}
C 5900 2400 1 0 0 gnd-1.sym
{
T 5900 2400 5 10 0 0 0 0 1
netname=GND
}
C 10100 4600 1 0 0 vcc-1.sym
{
T 10100 4600 5 10 0 0 0 0 1
netname=Vcc
}
C 5800 5800 1 0 0 vcc-1.sym
{
T 5800 5800 5 10 0 0 0 0 1
netname=Vcc
}
C 6200 5800 1 90 1 led-1.sym
{
T 6500 5300 5 10 1 1 0 6 1
refdes=D1
T 6200 5000 5 10 1 1 0 0 1
value=DLED_BL
T 6200 5800 5 10 0 0 0 0 1
file=../../lib/diode/led-backlight.mod
}
C 4600 3500 1 0 0 resistor-2.sym
{
T 4950 3750 5 10 1 1 0 0 1
refdes=R3
T 4900 3300 5 10 1 1 0 0 1
value=1.0K
}
C 10200 3100 1 0 0 gnd-1.sym
{
T 10200 3100 5 10 0 0 0 0 1
netname=GND
}
C 5500 3100 1 0 0 spice-npn-2.sym
{
T 6400 3800 5 8 0 0 0 0 1
device=NPN_TRANSISTOR
T 6100 3600 5 10 1 1 0 0 1
refdes=Q1
T 6100 3400 5 10 1 1 0 0 1
value=2N2222A
T 5500 3100 5 10 0 0 0 0 1
file=../../lib/bjt/2n2222a.mod
}
C 4200 3600 1 270 0 voltage-1.sym
{
T 4150 3250 5 10 1 1 180 0 1
refdes=Vin
T 5000 3550 5 10 0 1 180 0 1
value=DC 0.0
}
N 4400 3600 4600 3600 4
N 6000 3100 6000 2700 4
N 4400 2700 6000 2700 4
T 5600 100 9 10 1 0 0 0 1
1
T 6200 100 9 10 1 0 0 0 1
1
T 8700 100 9 10 1 0 0 0 1
Mike Waters
T 8700 400 9 10 1 0 0 0 1
20 / 01 / 2012
T 4800 400 9 10 1 0 0 0 1
backlight-ctrl.sch
C 6500 3900 1 0 0 output-2.sym
{
T 6700 4600 5 10 0 0 0 0 1
device=none
T 7400 3900 5 10 1 1 0 0 1
netname=Output
}
N 6000 4000 6500 4000 4
T 900 2700 9 10 1 0 0 0 17
Backlight LED :
Current     Voltage    
  (mA)          (V)
    10            3.5
    20            3.6
    50            3.7
  100            3.7
  200            3.8
  300            3.9
  400            3.9
  500            4.0

A LED current range of
200 - 300 mA seems ideal

Backlight current range :
80 - 650 mA

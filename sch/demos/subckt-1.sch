v 20150930 2
C 40000 40000 0 0 0 title-A4.sym
C 41000 47500 1 0 0 vcc-1.sym
C 41000 47500 1 270 0 voltage-3.sym
{
T 41700 47300 5 8 0 0 270 0 1
device=VOLTAGE_SOURCE
T 41500 47000 5 10 0 1 0 0 1
refdes=Vcc
T 40300 47000 5 10 1 1 0 0 1
value=DC 12V
}
C 41100 46300 1 0 0 gnd-1.sym
T 46400 40900 9 14 1 0 0 0 1
Sub-Circuit Demo. Circuit
T 44700 40400 9 10 1 0 0 0 1
subckt-1.sch
T 45500 40100 9 10 1 0 0 0 1
1
T 46200 40100 9 10 1 0 0 0 1
1
T 48600 40400 9 10 1 0 0 0 1
2018 - 11 - 05
T 48600 40100 9 10 1 0 0 0 1
Mike Waters
T 46200 45100 9 10 1 0 0 0 10
This example can be simulated using the following settings :

    Sim. Eng.   :  NG-Spice
    Probes       :  Vin & RL
    Analysis     :  Transient
    Start Time  :  0
    End Time   :  3 msec
    Step Inc.    :  1 usec
    Signal Src  :  None
    Init. Cond.  :  Cold or Warm
C 44300 44200 1 0 0 gnd-1.sym
N 42700 46800 43900 46800 4
C 44200 47500 1 0 0 vcc-1.sym
N 44400 47500 44400 47200 4
N 44400 44500 43000 44500 4
N 42700 46700 42700 46800 4
N 42700 45700 42700 45800 4
N 43300 45700 43300 45800 4
N 44400 45100 44400 44500 4
C 43500 45800 1 90 0 capacitor-1.sym
{
T 42800 46000 5 10 0 0 90 0 1
device=CAPACITOR
T 42600 46000 5 10 0 0 90 0 1
symversion=0.1
T 43400 46400 5 10 1 1 0 0 1
refdes=C3
T 43400 46100 5 10 1 1 180 6 1
value=47nF
}
N 43300 46700 43300 46800 4
N 44400 46000 44400 46400 4
C 43900 47200 1 180 1 pmos-2.sym
{
T 44600 47000 5 10 0 0 180 6 1
device=PMOS_TRANSISTOR
T 44000 47000 5 10 1 1 180 0 1
refdes=X2
T 44300 47100 5 10 1 1 0 6 1
value=IRF9Z34
}
N 44600 46800 44600 47200 4
N 44600 47200 44400 47200 4
C 42600 45800 1 270 1 resistor-2.sym
{
T 42500 46300 5 10 1 1 0 6 1
refdes=R4
T 42500 46200 5 10 1 1 180 0 1
value=330
}
N 42700 45700 43300 45700 4
C 42700 44500 1 0 0 vpulse-1.sym
{
T 43400 45350 5 10 0 0 0 0 1
device=vpulse
T 43400 45550 5 10 0 0 0 0 1
footprint=none
T 42900 44650 5 10 1 1 180 0 1
refdes=Vin
T 40300 44250 5 10 1 1 0 0 1
value=PULSE( 11.85 0.14 100u 100n 100n 34u 806u )
}
C 44500 46000 1 90 1 resistor-2.sym
{
T 44600 45600 5 10 1 1 0 0 1
refdes=RL
T 44600 45500 5 10 1 1 180 6 1
value=1K
}
C 40400 40300 1 0 0 spice-model-1.sym
{
T 40500 41000 5 10 0 1 0 0 1
device=model
T 40500 40900 5 10 1 1 0 0 1
refdes=A1
T 41700 40600 5 10 1 1 0 0 1
model-name=IRF9Z34
T 40900 40400 5 10 1 1 0 0 1
file=../../lib/models/irf9z34.ckt
}
T 46200 47300 9 10 1 0 0 0 3
This circuit was used as a test circuit for the IRF9Z34 which is
a P-channel power MOSFET. SPICE has a MOSFET model but
in this case X2 has been modelled using a sub-circuit.
T 46200 44000 9 10 1 0 0 0 4
In gSpiceUI preferences ensure that "Fix component prefixes"
option is not enabled or the X2 label will be changed to MX2
indicating a MOSFET SPICE model. In SPICE sub-circuit
designators must begin with the letter X.

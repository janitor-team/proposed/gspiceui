//**************************************************************************************************
//                                          PrcGWave.hpp                                           *
//                                         --------------                                          *
// Description : Provides a programatic interface to the utility gwave which is a wave form        *
//               viewer. The user can view the raw simulation data or envoke this utility which    *
//               plots the data graphically.                                                       *
// Started     : 2004-03-29                                                                        *
// Last Update : 2016-03-25                                                                        *
// Copyright   : (C) 2004-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PRCGWAVE_HPP
#define PRCGWAVE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PrcBase.hpp"

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class PrcGWave : public PrcBase
{
  private :

    wxFileName  m_oNameResults;  // The full simulator results file name

    // Functions for filtering the results file for input to the viewer
    bool  bFilterFile( void );

  public :

          PrcGWave( void );
         ~PrcGWave( );

    bool  bSetResultsFile( const wxString & rosFileName );

    const wxFileName & roGetResultsFile( void ) { return( m_oNameResults ); }

    virtual  bool  bExec( void );
};

//**************************************************************************************************

#endif // PRCGWAVE_HPP

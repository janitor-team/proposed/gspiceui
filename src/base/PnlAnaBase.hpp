//**************************************************************************************************
//                                         PnlAnaBase.hpp                                          *
//                                        ----------------                                         *
// Description : This class derives from the wxPanel class and provides a base class for all       *
//               analysis panel classes.                                                           *
// Started     : 2004-04-26                                                                        *
// Last Update : 2018-10-22                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLANABASE_HPP
#define PNLANABASE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "netlist/Component.hpp"
#include "utility/PnlValue.hpp"
#include "utility/StrUtils.hpp"

// wxWidgets Library Includes

#include <wx/gbsizer.h>

//**************************************************************************************************

class PnlAnaBase : public wxPanel
{
  protected :

    // The analysis type of the panel
    eTypeCmd        m_eAnaType;

    // An array containing the signal source components
    ArrayComponent  m_oaCpntSrcs;

    // Error message
    wxString        m_osErrMsg;

    // Sweep settings
#ifndef LAYOUT_MNGR
    wxStaticBox     m_oSbxSwpPars;
#endif // LAYOUT_MNGR
    PnlValue        m_oPnlStart;
    PnlValue        m_oPnlStop;
    PnlValue        m_oPnlStep;

    // Radio control used for various purposes depending on the analysis type
    wxRadioBox      m_oRbxSweep;

    // Parameters to be calculated
#ifndef LAYOUT_MNGR
    wxStaticBox     m_oSbxCalcPars;
#endif // LAYOUT_MNGR
    wxCheckBox      m_oCbxVoltage;
    wxCheckBox      m_oCbxCurrent;
    wxCheckBox      m_oCbxPower;
    wxCheckBox      m_oCbxResist;

    // Complex Parts
#ifndef LAYOUT_MNGR
    wxStaticBox     m_oSbxCpxPrt;
#endif // LAYOUT_MNGR
    wxCheckBox      m_oCbxMag;
    wxCheckBox      m_oCbxPhase;
    wxCheckBox      m_oCbxReal;
    wxCheckBox      m_oCbxImag;
    wxCheckBox      m_oCbxMagDb;

    // Input signal source, value and units
#ifndef LAYOUT_MNGR
    wxStaticBox     m_oSbxSigSrc;
#endif // LAYOUT_MNGR
    wxChoice        m_oChoSrcName;
    PnlValue        m_oPnlSrcLvl;
    wxButton        m_oBtnSetup;

    // .OPTIONS configuration dialog button
    wxButton        m_oBtnOPTIONS;

    // Analysis temperature
#ifndef LAYOUT_MNGR
    wxStaticBox     m_oSbxTemp;
#endif // LAYOUT_MNGR
    PnlValue        m_oPnlTemp;

    // Functions to create the display objects
    virtual  void  CreateBase  ( void );
    virtual  void  CreateScale ( void );
    virtual  void  CreateInitC ( void );
    virtual  void  CreateSigSrc( void );
    virtual  void  CreateCpxPrt( void );
    virtual  void  CreateTemp  ( void );

#ifdef LAYOUT_MNGR
    virtual  void  DoLayout( void );
#endif // LAYOUT_MNGR

    virtual  void  InitScale( void ) { }
    virtual  bool  bSetScale( eTypeScale eScale );

             bool  bSetInitC   ( eTypeInitC eInitC );
             void  LoadSrcNames( ArrayComponent & roaCpnts, wxString osPrefixes );
             bool  bSetSrcCpnt ( Component & roCpnt );
             void  ToolTips    ( void );                      // ??? Not yet implemented

  public :

    explicit           PnlAnaBase( wxWindow * poWin );
                      ~PnlAnaBase( );

    virtual  bool      bClear( void );
             bool      bIsOk ( void )                        { return( m_osErrMsg.IsEmpty( ) ); }

             bool      bSetAnalysType( eTypeCmd eAnaType );
             eTypeCmd  eGetAnalysType( void )                { return( m_eAnaType ); }

#ifdef LAYOUT_MNGR
             bool      bSetSwpParsLbl( const wxString & rosLabel );
#endif

    // Get or set the error message
    const wxString & rosGetErrMsg( void )                    { return( m_osErrMsg ); }
          void          SetErrMsg( const wxString & rosMsg ) { if( bIsOk( ) ) m_osErrMsg = rosMsg; }

    // Declare friend classes
    friend  class  NbkSimrBase;
    friend  class  NbkGnuCap;
    friend  class  NbkNgSpice;

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum ePnlItemID
    {
      ID_PNL_START = 1,
      ID_PNL_STOP,
      ID_PNL_STEP,

      ID_RBX_SWEEP,
      ID_RBX_SCALE,
      ID_RBX_INITC,

      ID_CHO_SRCNAME,
      ID_PNL_SRCLVL,

      ID_CBX_MAG,
      ID_CBX_PHASE,
      ID_CBX_REAL,
      ID_CBX_IMAG,
      ID_CBX_MAGDB,

      ID_CBX_VOLT,
      ID_CBX_CURR,
      ID_CBX_PWR,
      ID_CBX_RES,

      ID_BTN_OPTIONS,

      ID_PNL_TEMP,

      ID_UNUSED,        // Assigned to controls for which events are not used

      ID_FST = ID_PNL_START,
      ID_LST = ID_PNL_TEMP
    };
};

//**************************************************************************************************

#endif // PNLANABASE_HPP

v 20150930 2
C 40000 40000 0 0 0 title-A3.sym
C 52000 43950 1 0 1 vdc-1.sym
{
T 51300 44500 5 10 0 1 0 6 1
refdes=V1
T 51300 44450 5 10 1 1 0 6 1
value=DC +70V
}
C 51400 43450 1 180 1 vdc-1.sym
{
T 51000 42950 5 10 0 1 180 6 1
refdes=V2
T 50600 42900 5 10 1 1 180 6 1
value=DC -70V
}
C 51125 43500 1 0 0 gnd-1.sym
T 49400 40900 9 14 1 0 0 0 1
BJT Differential Amplifier
T 49600 40400 9 10 1 0 0 0 1
bjt-diff-amp.sch
T 50400 40100 9 10 1 0 0 0 1
1
T 51100 40100 9 10 1 0 0 0 1
1
T 53500 40400 9 10 1 0 0 0 1
2017 - 05 - 27
T 53500 40100 9 10 1 0 0 0 1
Mike Waters
C 51900 42250 1 180 0 vee-1.sym
C 51500 45150 1 0 0 vcc-1.sym
N 51700 43950 51700 43450 4
N 51700 43800 51225 43800 4
T 63500 50200 8 10 0 0 270 0 1
device=INDUCTOR
T 63700 50200 8 10 0 0 270 0 1
symversion=0.1
C 53000 41800 1 0 0 spice-model-1.sym
{
T 53100 42400 5 10 1 1 0 0 1
refdes=A7
T 54300 42100 5 10 1 1 0 0 1
model-name=1N4148
T 53500 41900 5 10 1 1 0 0 1
file=../../lib/models/1n4148.mod
}
C 44100 50400 1 90 1 voltage-1.sym
{
T 43600 49900 5 10 1 1 0 6 1
refdes=Vin
T 43300 50350 5 10 0 1 180 6 1
value=DC 30.0
}
C 44700 41900 1 180 0 vee-1.sym
C 45100 49400 1 0 0 vcc-1.sym
C 43700 44700 1 270 0 diode-1.sym
{
T 44200 44100 5 10 1 1 0 0 1
refdes=D1
T 43950 43900 5 10 1 1 0 0 1
value=1N4148
}
C 43700 43800 1 270 0 diode-1.sym
{
T 44200 43200 5 10 1 1 0 0 1
refdes=D2
T 43950 43000 5 10 1 1 0 0 1
value=1N4148
}
C 43700 42900 1 270 0 diode-1.sym
{
T 44200 42300 5 10 1 1 0 0 1
refdes=D3
T 43950 42100 5 10 1 1 0 0 1
value=1N4148
}
C 44000 45800 1 90 1 resistor-2.sym
{
T 44050 45400 5 10 1 1 0 0 1
refdes=R8
T 44050 45200 5 10 1 1 0 0 1
value=10K
}
N 43900 46200 43900 45800 4
N 43900 44900 43900 44700 4
C 45400 43700 1 90 1 resistor-2.sym
{
T 45550 43300 5 10 1 1 0 0 1
refdes=R9
T 45550 43100 5 10 1 1 0 0 1
value=390
}
N 45300 44200 45300 43700 4
N 44800 44700 43900 44700 4
N 45300 42800 45300 41900 4
N 45300 45100 45300 45800 4
C 44700 46700 1 90 1 resistor-2.sym
{
T 44750 46300 5 10 1 1 0 0 1
refdes=R6
T 44750 46100 5 10 1 1 0 0 1
value=150
}
C 45900 46700 1 270 0 resistor-2.sym
{
T 45850 46300 5 10 1 1 0 6 1
refdes=R7
T 45850 46100 5 10 1 1 0 6 1
value=150
}
N 44600 45800 46000 45800 4
C 43800 49000 1 270 0 resistor-2.sym
{
T 43750 48600 5 10 1 1 0 6 1
refdes=R2
T 43750 48400 5 10 1 1 0 6 1
value=220
}
N 43900 47200 44100 47200 4
C 44700 49100 1 90 1 resistor-2.sym
{
T 44750 48700 5 10 1 1 0 0 1
refdes=R4
T 44750 48500 5 10 1 1 0 0 1
value=6.8K
}
C 45900 49100 1 270 0 resistor-2.sym
{
T 45850 48700 5 10 1 1 0 6 1
refdes=R5
T 45850 48500 5 10 1 1 0 6 1
value=6.8K
}
N 44600 48200 44600 47600 4
N 46000 47600 46000 48200 4
C 46800 49000 1 90 1 resistor-2.sym
{
T 46850 48600 5 10 1 1 0 0 1
refdes=R3
T 46850 48400 5 10 1 1 0 0 1
value=220
}
N 46500 47200 46700 47200 4
N 44600 49400 46000 49400 4
C 44100 46700 1 0 0 spice-npn-2.sym
{
T 45000 47400 5 8 0 0 0 0 1
device=NPN_TRANSISTOR
T 44700 47300 5 10 1 1 0 0 1
refdes=Q1
T 44450 47100 5 10 1 1 0 0 1
value=BF471
}
C 44800 44200 1 0 0 spice-npn-2.sym
{
T 45700 44900 5 8 0 0 0 0 1
device=NPN_TRANSISTOR
T 45400 44800 5 10 1 1 0 0 1
refdes=Q10
T 45150 44600 5 10 1 1 0 0 1
value=BF471
}
C 46500 46700 1 0 1 spice-npn-2.sym
{
T 45600 47400 5 8 0 0 0 6 1
device=NPN_TRANSISTOR
T 45900 47300 5 10 1 1 0 6 1
refdes=Q2
T 46150 47100 5 10 1 1 0 6 1
value=BF471
}
N 43900 41900 45300 41900 4
N 44800 50400 46700 50400 4
N 46700 50400 46700 49000 4
C 43900 50300 1 0 0 resistor-2.sym
{
T 44200 50700 5 10 1 1 180 6 1
refdes=Rin
T 44200 50100 5 10 1 1 0 0 1
value=100
}
C 53000 43800 1 0 0 spice-model-1.sym
{
T 53100 44400 5 10 1 1 0 0 1
refdes=A5
T 54300 44100 5 10 1 1 0 0 1
model-name=BF471
T 53500 43900 5 10 1 1 0 0 1
file=../../lib/models/bf471.mod
}
C 53000 42800 1 0 0 spice-model-1.sym
{
T 53100 43400 5 10 1 1 0 0 1
refdes=A6
T 54300 43100 5 10 1 1 0 0 1
model-name=BF472
T 53500 42900 5 10 1 1 0 0 1
file=../../lib/models/bf472.mod
}
N 46000 47700 47400 47700 4
{
T 46000 47700 5 10 0 0 0 0 1
netname=Output
}
C 43200 45800 1 0 0 gnd-1.sym
T 50400 50000 9 10 1 0 0 0 6
Transient Analysis :
    Set signal soure : 1kHz, 10mV, -5.75 DC offset
                                 MOS_Drv quiescent = approx. 0.8V
                                 Gain = 3130.6 = 69.9 dBV
    At signal source amplitudes greater than 10mV the output
    waveform becomes distorted (Vout = 31V amplitude).
T 50400 48900 9 10 1 0 0 0 4
AC Analysis :
    Gain         = 81.3 dBV
    3dB Band = DC to   2.0 KHz @ with      C7 and C8
    3dB Band = DC to 47.9 KHz @ without C7 and C8
N 43300 46100 43900 46100 4
T 53000 45400 9 10 1 0 0 0 3
This schematic can be simulated
using NG-Spice but not GNU-Cap
(last tested 2011-02-04)
C 47400 47600 1 0 0 output-2.sym
{
T 47600 48300 5 10 0 0 0 0 1
device=none
T 47600 47900 5 10 1 1 0 0 1
description=Output
}
T 50400 48200 9 10 1 0 0 0 2
Current source formed by Q9   is approx. 5mA
Current source formed by Q10 is approx. 5mA
T 51600 46700 9 10 1 0 0 0 4
Suggest a value for R21, R22, R31 and R32 of
22kOhm which increases the input impedance
from 24.3kOhm to 46.3kOhm but only reduces
the gain from 75.6dBV to 70.0dBV.
N 44600 49100 44600 49400 4
N 46000 49100 46000 49400 4
N 43900 49500 43900 49000 4
N 43900 42000 43900 41900 4
N 43900 48100 43900 47100 4
N 46700 48100 46700 47100 4
C 43800 46200 1 270 1 resistor-2.sym
{
T 43750 46800 5 10 1 1 180 0 1
refdes=R1
T 43750 46600 5 10 1 1 180 0 1
value=10K
}
C 46800 46200 1 90 0 resistor-2.sym
{
T 46850 46800 5 10 1 1 180 6 1
refdes=R10
T 46850 46600 5 10 1 1 180 6 1
value=10K
}
C 46600 45800 1 0 0 gnd-1.sym
N 46700 46100 46700 46200 4

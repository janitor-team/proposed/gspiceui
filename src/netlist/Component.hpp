//**************************************************************************************************
//                                          Component.hpp                                          *
//                                         ---------------                                         *
// Description : This class processes a component definition line from a netlist. The line is      *
//               parsed and it's constituent fields extracted.                                     *
// Started     : 2004-05-14                                                                        *
// Last Update : 2018-10-30                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef COMPONENT_HPP
#define COMPONENT_HPP

// Application Include Files

#include "TypeDefs.hpp"
#include "utility/CnvtType.hpp"
#include "utility/StrUtils.hpp"

// wxWidgets Include Files

#include <wx/tokenzr.h>

//**************************************************************************************************

class Component : public wxString
{
  protected :

    // The following attributes are the minumum required to define a component
    wxString       m_osName;
    wxArrayString  m_osaNodes;
    wxString       m_osValue;

    // The following attributes are derived from the above attributes
    eTypeCpnt      m_eType;
    wxArrayString  m_osaPorts;                               // ??? Not yet implemented

    // Error message string
    wxString       m_osErrMsg;

    virtual  bool  bParseValue ( void ) { return( true ); }  // ??? Not yet implemented
    virtual  bool  bFormatValue( void ) { return( true ); }  // ??? Not yet implemented
    virtual  bool  bValidate   ( void );

             void  GetPorts( void );                         // ??? Not yet implemented

  public :

    // Function which provides the Component sorting criterion
    static   int   iCompare( Component ** ppo1, Component ** ppo2 );

                   Component( void );
                   Component( const Component & roCpnt );
    virtual       ~Component( );

    virtual  bool  bClear  ( void );
             bool  bIsValid( void ) { return( m_osErrMsg.IsEmpty( ) ); }

    virtual  bool  bParse ( void );
    virtual  bool  bFormat( void );

             bool  bSetString( const wxString & rosCmd    );
             bool  bSetName  ( const wxString & rosName   );
             bool  bSetNodes ( const wxString & rosNodes  );
             bool  bSetValue ( const wxString & rosValue  );
             bool  bSetErrMsg( const wxString & rosErrMsg );

            const  wxString      &  rosGetString   ( void );
    inline  const  wxString      &  rosGetName     ( void ) { return( m_osName   ); }
    inline  const  wxArrayString & rosaGetNodes    ( void ) { return( m_osaNodes ); }
    inline  const  wxString      &  rosGetValue    ( void ) { return( m_osValue  ); }
                   eTypeCpnt          eGetType     ( void ) { return( m_eType    ); }
            const  wxString      &  rosGetNodes    ( void );
            const  wxString      &  rosGetNumValue ( void );
                   eTypeUnits         eGetUnitsType( void ) { return( eGetUnitsType( m_osName ) ); }
    inline  const  wxString      &  rosGetErrMsg   ( void ) { return( m_osErrMsg ); }

    // Static helper functions (may be called without instantiating the class)
    static         eTypeCpnt   eGetType     ( const wxString & rosName );
    static         eTypeUnits  eGetUnitsType( const wxString & rosName );
    static  const  wxString &  rosTrim      ( const wxString & rosDefn );

    wxString  & operator = ( const wxString  & rosCmd );
    Component & operator = ( const Component & roCpnt );

    virtual  void  Print( const wxString & rosPrefix=wxT("  ") );
};

//**************************************************************************************************
// Define a new array type

WX_DECLARE_OBJARRAY( Component, ArrayComponent );

//**************************************************************************************************

#endif // COMPONENT_HPP

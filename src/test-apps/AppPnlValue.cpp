//**************************************************************************************************
//                                        AppPnlValue.cpp                                          *
//                                       -----------------                                         *
// Started     : 2014-11-17                                                                        *
// Last Update : 2015-01-24                                                                        *
// Copyright   : (C) 2014-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "AppPnlValue.hpp"

//**************************************************************************************************

wxIMPLEMENT_APP( AppPnlValue );

wxBEGIN_EVENT_TABLE( FrmMain, wxFrame )

  EVT_MENU  ( FrmMain::ID_BTN_NEXT, FrmMain::OnNext  )
  EVT_MENU  (        wxID_EXIT    , FrmMain::OnExit  )
  EVT_MENU  (        wxID_ABOUT   , FrmMain::OnAbout )

  EVT_BUTTON( FrmMain::ID_BTN_NEXT, FrmMain::OnNext  )
  EVT_BUTTON( FrmMain::ID_BTN_READ, FrmMain::OnRead  )
  EVT_BUTTON( FrmMain::ID_BTN_QUIT, FrmMain::OnExit  )

wxEND_EVENT_TABLE( )

//**************************************************************************************************
//                                    Class AppPnlValue
//**************************************************************************************************

bool  AppPnlValue::OnInit( )
{
  FrmMain * poFrmMain = new FrmMain( wxT("test_AppPnlValue"), wxPoint( 83, 83 ), wxDefaultSize );

  poFrmMain->Show( true );

  return( true );
}

//**************************************************************************************************
//                                    Class FrmMain
//**************************************************************************************************

FrmMain::FrmMain( const wxString & osTitle, const wxPoint & oPosn, const wxSize & oSize )
                : wxFrame( NULL, wxID_ANY, osTitle, oPosn, oSize )
{
  wxCommandEvent  oNonEvent;

  // Create the GUI
  Create( );
  DoLayout( );

  // Load test values
  oVecTests.push_back( wxT("10.0E-12F") ); // Working 2015-01-24
  oVecTests.push_back( wxT("1.0V")      ); // Working 2015-01-24
  oVecTests.push_back( wxT("1000.0mV")  ); // Working 2015-01-24
  oVecTests.push_back( wxT("1.0fV")     ); // Working 2015-01-24
  oVecTests.push_back( wxT("0.0mV")     ); // Working 2015-01-24
  oVecTests.push_back( wxT("140mV")     ); // Working 2015-01-24
  oVecTests.push_back( wxT("1.0pA")     ); // Working 2015-01-24
  oVecTests.push_back( wxT("1.0E-12A")  ); // Working 2015-01-24
  oVecTests.push_back( wxT("1000GOhm")  ); // Working 2015-01-24
  oVecTests.push_back( wxT("1000Ohm")   ); // Working 2015-01-24
  oVecTests.push_back( wxT("10.0fC")    ); // Working 2015-01-24
  oVecTests.push_back( wxT("1234")      ); // Working 2015-01-24
  oVecTests.push_back( wxT("1.235")     ); // Working 2015-01-24
  oVecTests.push_back( wxT("1.0E-12")   ); // Working 2015-01-24

  // Load the first test value
  OnNext( oNonEvent );
}

//**************************************************************************************************

void  FrmMain::Create( void )
{
  wxMenuBar * poMenuBar  = new wxMenuBar;
  wxMenu    * poMenuFile = new wxMenu;
  wxMenu    * poMenuHelp = new wxMenu;
  wxPanel   * poPnlMain  = new wxPanel( this );
  wxPanel   * poPnlCtls  = new wxPanel( poPnlMain );
  wxPanel   * poPnlBtns  = new wxPanel( poPnlMain );

  // Create the menu items
  poMenuFile->Append( ID_BTN_NEXT, wxT("&Next...\tCtrl-N") );
  poMenuFile->AppendSeparator( );
  poMenuFile->Append( wxID_EXIT );
  poMenuHelp->Append( wxID_ABOUT );

  // Create the menu bar
  poMenuBar->Append( poMenuFile, wxT("&File") );
  poMenuBar->Append( poMenuHelp, wxT("&Help") );
  SetMenuBar( poMenuBar );

  // Create the text control to contain the value written to the PnlValue test control
  m_oTxtInput.bCreate( poPnlCtls, ID_TXT_INPUT  , 150, 163 );
  m_oTxtInput.bSetName( wxT("PnlValue input value") );

  // Create the PnlValue test control
  m_oPnlValue.bCreate( poPnlCtls, ID_VAL_TEST   , 150, 90, 70 );
  m_oPnlValue.bSetName( wxT("PnlValue test control") );

  // Create the text control to contain the value read from the PnlValue test control
  m_oTxtOutput.bCreate( poPnlCtls, ID_TXT_OUTPUT, 150, 163 );
  m_oTxtOutput.bSetName( wxT("PnlValue output value") );

  // Create the button control
  m_oBtnNext.Create( poPnlBtns, ID_BTN_NEXT, wxT("Next") );
  m_oBtnRead.Create( poPnlBtns, ID_BTN_READ, wxT("Read") );
  m_oBtnQuit.Create( poPnlBtns, ID_BTN_QUIT, wxT("Quit") );
}

//**************************************************************************************************

void  FrmMain::DoLayout( void )
{
  wxPanel     * poPnlCtls = (wxPanel *) m_oPnlValue.GetParent( );
  wxPanel     * poPnlBtns = (wxPanel *) m_oBtnNext .GetParent( );
  wxPanel     * poPnlMain = (wxPanel *) poPnlCtls ->GetParent( );
  wxBoxSizer  * poSzrMain = new wxBoxSizer      ( wxVERTICAL );
  wxBoxSizer  * poSzrCtls = new wxStaticBoxSizer( wxVERTICAL, poPnlCtls );
  wxBoxSizer  * poSzrBtns = new wxBoxSizer      ( wxHORIZONTAL );
  wxSizerFlags  oFlags;

  // Set the sizer
  poPnlMain->SetSizer( poSzrMain );
  poPnlCtls->SetSizer( poSzrCtls );
  poPnlBtns->SetSizer( poSzrBtns );

  // Layout the controls
  oFlags.Align( wxALIGN_LEFT );
  oFlags.Border( wxLEFT | wxRIGHT | wxTOP   , 10 );
  poSzrCtls->Add( &m_oTxtInput , oFlags );
  oFlags.Border( wxLEFT | wxRIGHT           , 10 );
  poSzrCtls->Add( &m_oPnlValue , oFlags );
  oFlags.Border( wxLEFT | wxRIGHT | wxBOTTOM, 10 );
  poSzrCtls->Add( &m_oTxtOutput, oFlags );

  // Layout the buttons
  oFlags.Border( wxTOP, 15 );
  oFlags.Align( wxALIGN_RIGHT );
  poSzrBtns->Add( &m_oBtnNext, oFlags );
  poSzrBtns->AddSpacer( 15 );
  oFlags.Align( wxALIGN_CENTER );
  poSzrBtns->Add( &m_oBtnRead, oFlags );
  poSzrBtns->AddSpacer( 15 );
  oFlags.Align( wxALIGN_LEFT );
  poSzrBtns->Add( &m_oBtnQuit, oFlags );

  // Layout the main frame
  oFlags.Border( wxLEFT | wxRIGHT | wxTOP, 15 );
  poSzrMain->Add( poPnlCtls, oFlags );
  oFlags.Border( wxBOTTOM                , 15 );
  oFlags.Align( wxALIGN_CENTER );
  poSzrMain->Add( poPnlBtns, oFlags );

  // Set minimum size and initial size as calculated by the sizer
  poSzrMain->SetSizeHints( this );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************

void  FrmMain::OnNext( wxCommandEvent & roEvent )
{
  static  size_t      sz1=oVecTests.size( );
          eTypeUnits  eUType;
          wxString    os1;
          double      df1;

  // Increment the index to the next PnlValue value
  if( ++sz1 >= oVecTests.size( ) ) sz1 = 0;

  // Get the PnlValue value
  os1 = oVecTests[ sz1 ];

  // Set the new PnlValue contents
  m_oTxtInput.bSetText( os1 );

  // Decide how the PnlValue should configured to display this value
  m_oPnlValue.bClear( );
  if( UnitsBase::bGetUnitsType( os1, &eUType ) )
  { // Display a units choice box
    m_oPnlValue.bShowUnits( PnlValue::eSHOW_CHO );
    m_oPnlValue.bSetUnitsType( eUType );
  }
  else
  { // Display a units label
    m_oPnlValue.bShowUnits( PnlValue::eSHOW_LBL );
    if(      CnvtType::bIsInteger( os1 ) )
    { // Display the value as an integer
      m_oPnlValue.bSetValueType( eVALUE_INT );
      m_oPnlValue.bSetSpnRange( 0, 10000 );
      m_oPnlValue.bSetSpnIncSz( 1,  1000 );
    }
    else if( CnvtType::bIsFloat( os1 ) )
    { // Display the value as a float
      CnvtType::bStrToFlt( os1, &df1 );
      if( (df1>0.1 && df1<1000.0) || df1==0.0 )
           m_oPnlValue.bSetValueType( eVALUE_FLT );
      else m_oPnlValue.bSetValueType( eVALUE_SCI );
    }
  }

  // Display the value
  m_oPnlValue.bSetValue( os1 );
}

//**************************************************************************************************

void  FrmMain::OnRead( wxCommandEvent & roEvent )
{
  m_oTxtOutput.bSetText( m_oPnlValue.rosGetValue( ) + m_oPnlValue.rosGetUnits( ) );
}

//**************************************************************************************************

void  FrmMain::OnAbout( wxCommandEvent & roEvent )
{
  wxString os1;

  os1 << wxT("Test Application for : \n\n")
      << wxT("C++ class PnlValue and all  \n")
      << wxT("classes it makes use of.\n\n")
      << wxT("Version 1.07 (2015-01-19)");

  wxMessageBox( os1, wxT("About"), wxOK | wxICON_INFORMATION );
}

//**************************************************************************************************

void  FrmMain::OnExit( wxCommandEvent & roEvent )
{
  Close( true );
}

//**************************************************************************************************

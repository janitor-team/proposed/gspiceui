//**************************************************************************************************
//                                          TypeDefs.cpp                                           *
//                                         --------------                                          *
// Started     : 2009-05-07                                                                        *
// Last Update : 2018-10-22                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "TypeDefs.hpp"

//**************************************************************************************************
// The following completes the definition of new array types. It expands into some C++ code and so
// should only be compiled once (ie. don't put this into a header file rather put it in a source
// file or there will be linker errors).

#include <wx/arrimpl.cpp>

WX_DEFINE_OBJARRAY( ArrayFileName )

//**************************************************************************************************
// Enumerated type to string conversion functions.

wxString & rosEnumEngToStr( eTypeSimEng eSimEng )
{
  static  wxString  osSimEng;

  switch( eSimEng )
  {
    case eSIMR_NGSPICE : osSimEng = wxT("NG-Spice"); break;
    case eSIMR_GNUCAP  : osSimEng = wxT("GNU-Cap");  break;
    default            : osSimEng = wxT("None");
  }

  return( osSimEng );
}

//--------------------------------------------------------------------------------------------------

wxString & rosEnumEdaToStr( eTypeEDA eEDA )
{
  static  wxString  osEDA;

  switch( eEDA )
  {
    case eEDA_LEPTON : osEDA = wxT("Lepton-EDA"); break;
    case eEDA_GEDA   : osEDA = wxT("gEDA");       break;
    default          : osEDA = wxT("None");
  }

  return( osEDA );
}

//--------------------------------------------------------------------------------------------------

wxString & rosEnumVwrToStr( eTypeViewer eViewer )
{
  static  wxString  osViewer;

  switch( eViewer )
  {
    case eVIEW_GAW     : osViewer = wxT("Gaw");      break;
    case eVIEW_GWAVE   : osViewer = wxT("Gwave");    break;
//  case eVIEW_GNUPLOT : osViewer = wxT("GNU-Plot"); break;
    default          : osViewer = wxT("None");
  }

  return( osViewer );
}

//--------------------------------------------------------------------------------------------------

wxString & rosEnumCmdToStr( eTypeCmd eCmd )
{
  static  wxString  osCmd;

  switch( eCmd )
  {
    case eCMD_OP : osCmd = wxT("OP"); break;
    case eCMD_DC : osCmd = wxT("DC"); break;
    case eCMD_AC : osCmd = wxT("AC"); break;
    case eCMD_TR : osCmd = wxT("TR"); break;
//  case eCMD_FO : osCmd = wxT("FO"); break;
//  case eCMD_DI : osCmd = wxT("DI"); break;
//  case eCMD_NO : osCmd = wxT("NO"); break;
//  case eCMD_PZ : osCmd = wxT("PZ"); break;
//  case eCMD_SE : osCmd = wxT("SE"); break;
//  case eCMD_TF : osCmd = wxT("TF"); break;
    default      : osCmd = wxT("None");
  }

  return( osCmd );
}

//**************************************************************************************************
// String to enumerated type conversion functions.

eTypeSimEng  eStrToEnumEng( const wxString & rosSimEng )
{
  eTypeSimEng  eSimEng;
  wxString     os1;

  os1 = rosSimEng.Upper( );

  if(      os1 == wxT("NG-SPICE") ) eSimEng = eSIMR_NGSPICE;
  else if( os1 == wxT("NGSPICE")  ) eSimEng = eSIMR_NGSPICE;
  else if( os1 == wxT("GNU-CAP")  ) eSimEng = eSIMR_GNUCAP;
  else if( os1 == wxT("GNUCAP")   ) eSimEng = eSIMR_GNUCAP;
  else                              eSimEng = eSIMR_NONE;

  return( eSimEng );
}

//--------------------------------------------------------------------------------------------------

eTypeEDA  eStrToEnumEda( const wxString & rosEDA )
{
  eTypeEDA  eEDA;
  wxString  os1;

  os1 = rosEDA.Upper( );

  if(      os1 == wxT("LEPTON-EDA") ) eEDA = eEDA_LEPTON;
  else if( os1 == wxT("GEDA")       ) eEDA = eEDA_GEDA;
  else                                eEDA = eEDA_NONE;

  return( eEDA );
}

//--------------------------------------------------------------------------------------------------

eTypeViewer  eStrToEnumVwr( const wxString & rosViewer )
{
  eTypeViewer  eViewer;
  wxString     os1;

  os1 = rosViewer.Upper( );

  if(      os1 == wxT("GAW")      ) eViewer = eVIEW_GAW;
  else if( os1 == wxT("GWAVE")    ) eViewer = eVIEW_GWAVE;
//else if( os1 == wxT("GNU-PLOT") ) eViewer = eVIEW_GNUPLOT;
  else                              eViewer = eVIEW_NONE;

  return( eViewer );
}

//--------------------------------------------------------------------------------------------------

eTypeCmd  eStrToEnumCmd( const wxString & rosCmd )
{
  eTypeCmd  eCmd;
  wxString  os1;

  os1 = rosCmd.Upper( );

  if(      os1 == wxT("OP") ) eCmd = eCMD_OP;
  else if( os1 == wxT("DC") ) eCmd = eCMD_DC;
  else if( os1 == wxT("AC") ) eCmd = eCMD_AC;
  else if( os1 == wxT("TR") ) eCmd = eCMD_TR;
//else if( os1 == wxT("FO") ) eCmd = eCMD_FO;
//else if( os1 == wxT("DI") ) eCmd = eCMD_DI;
//else if( os1 == wxT("NO") ) eCmd = eCMD_NO;
//else if( os1 == wxT("PZ") ) eCmd = eCMD_PZ;
//else if( os1 == wxT("SE") ) eCmd = eCMD_SE;
//else if( os1 == wxT("TF") ) eCmd = eCMD_TF;
  else                        eCmd = eCMD_NONE;

  return( eCmd );
}

//**************************************************************************************************

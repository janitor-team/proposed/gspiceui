//**************************************************************************************************
//                                        CmdNgSpiceAC.cpp                                         *
//                                       ------------------                                        *
// Started     : 2007-10-11                                                                        *
// Last Update : 2018-09-25                                                                        *
// Copyright   : (C) 2007-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "CmdNgSpiceAC.hpp"

//**************************************************************************************************
// Constructor.

CmdNgSpiceAC::CmdNgSpiceAC( void ) : CmdBase( )
{
  bSetDefaults( );
}

//**************************************************************************************************
// Destructor.

CmdNgSpiceAC::~CmdNgSpiceAC( )
{
}

//**************************************************************************************************
// Check that the object attributes are valid.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdNgSpiceAC::bValidate( void )
{
  double  df1, df2, df3;

  // Call the base class validation function first
  CmdBase::bValidate( );

  // Check the sweep start and stop values
  if( ! CnvtType::bStrToFlt( m_osStart, &df1 ) ) SetErrMsg( wxT("Invalid sweep start value.") );
  if( ! CnvtType::bStrToFlt( m_osStop,  &df2 ) ) SetErrMsg( wxT("Invalid sweep stop value.") );
  if( df2 < df1 )                                SetErrMsg( wxT("Start value greater than stop value.") );

  // Check the sweep step value and sweep scale
  if( ! CnvtType::bStrToFlt( m_osStep, &df3 ) )  SetErrMsg( wxT("Invalid sweep step value.") );
  if( df3 <= 0.0 )                               SetErrMsg( wxT("Step value less than or equal to zero.") );
  switch( m_eScale )
  {
    case eSCALE_LIN :
      if( df1!=df2 && df3>fabs( df2 - df1 ) )    SetErrMsg( wxT("Step value is greater than sweep range.") );
      break;
    case eSCALE_DEC :
    case eSCALE_OCT :
      break;
    default         :                            SetErrMsg( wxT("Invalid step scale value.") );
  }

  return( bIsValid( ) );
}

//**************************************************************************************************
// Set the object attributes to they're default values.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdNgSpiceAC::bSetDefaults( void )
{
  CmdBase::bSetDefaults( );

  m_eSimEng  = eSIMR_NGSPICE;
  m_eCmdType = eCMD_AC;

  m_osStart  = wxT("1.0K");
  m_osStop   = wxT("100.0K");
  m_osStep   = wxT("10");

  m_eScale   = eSCALE_DEC;

  return( true );
}

//**************************************************************************************************
// Parse the command string.
//
// Eg.s : .AC LIN 50 1.00K 300.00K
//        .AC DEC 20 1.00K 300.00K
//        .AC OCT 10 1.00K 300.00K
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdNgSpiceAC::bParse( void )
{
  wxStringTokenizer  ostk1;
  wxString           os1;
  double             df1;
  long               li1;

  // Clear the object attributes
  os1 = *this;
  bSetDefaults( );
  assign( os1 );

  // Tokenize the command string
  ostk1.SetString( *this );
  if( ostk1.CountTokens( ) != 5 ) return( bValidate( ) );

  // Check command type
  os1 = ostk1.GetNextToken( ).Left( 3 ).Upper( );
  if( os1 != wxT(".AC") )         return( bValidate( ) );

  // Extract the sweep type: linear or log
  os1 = ostk1.GetNextToken( ).Upper( );
  if(      os1 == wxT("LIN") ) m_eScale = eSCALE_LIN;
  else if( os1 == wxT("DEC") ) m_eScale = eSCALE_DEC;
  else if( os1 == wxT("OCT") ) m_eScale = eSCALE_OCT;
  else                            return( bValidate( ) );

  // Extract the step count
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToInt( os1, &li1 ) ) m_osStep  = os1;
  else                            return( bValidate( ) );

  // Extract the start frequency
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osStart = os1;
  else                            return( bValidate( ) );

  // Extract the stop frequency
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osStop  = os1;
  else                            return( bValidate( ) );

  return( bValidate( ) );
}

//**************************************************************************************************
// Format the command string.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdNgSpiceAC::bFormat( void )
{
  wxString  osCmd, os1;

  osCmd = wxT(".AC");

  switch( m_eScale )
  {
    case eSCALE_LIN : osCmd << wxT(" LIN"); break;
    case eSCALE_DEC : osCmd << wxT(" DEC"); break;
    case eSCALE_OCT : osCmd << wxT(" OCT"); break;
    default         : break;
  }

  osCmd << wxT(' ') << m_osStep;
  osCmd << wxT(' ') << m_osStart;
  osCmd << wxT(' ') << m_osStop;

  assign( osCmd );

  return( bValidate( ) );
}

//**************************************************************************************************
// Copy the contents of a CmdGnuCapAC object.
//
// Argument List :
//   poCmdAC - A pointer to a CmdNgSpiceAC object
//
// Return Values :
//   A pointer to this object

CmdNgSpiceAC & CmdNgSpiceAC::operator = ( const CmdGnuCapAC & roCmdAC )
{
  (CmdBase &) *this = (CmdBase &) roCmdAC;

  m_osStart = roCmdAC.m_osStart;
  m_osStop  = roCmdAC.m_osStop;
  m_osStep  = roCmdAC.m_osStep;
  m_eScale  = roCmdAC.m_eScale;

  // NG-Spice doesn't support logarithmic scaling
  if( m_eScale == eSCALE_LOG ) m_eScale = eSCALE_DEC;

  bFormat( );

  return( *this );
}

//**************************************************************************************************
// Print the object attributes.
//
// Argument List :
//   psPrefix - A prefix to every line displayed (usually just spaces)

void  CmdNgSpiceAC::Print( const wxString & rosPrefix )
{
  CmdBase::Print( rosPrefix + wxT("CmdBase::") );

  std::cout << rosPrefix.mb_str( ) << "m_osStart : " << m_osStart .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osStop  : " << m_osStop  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osStep  : " << m_osStep  .mb_str( ) << '\n';

  std::cout << rosPrefix.mb_str( ) << "m_eScale  : ";
  switch( m_eScale )
  {
    case eSCALE_LIN  : std::cout << "eSCALE_LIN\n";  break;
    case eSCALE_LOG  : std::cout << "eSCALE_LOG\n";  break;
    case eSCALE_DEC  : std::cout << "eSCALE_DEC\n";  break;
    case eSCALE_OCT  : std::cout << "eSCALE_OCT\n";  break;
    case eSCALE_NONE : std::cout << "eSCALE_NONE\n"; break;
    default :          std::cout << "Invalid\n";     break;
  }
}

//**************************************************************************************************
//                                          Test Utility                                           *
//**************************************************************************************************

#ifdef TEST_CMDNGSPICEAC

using  namespace  std;

// Function prototypes

void  Usage( char * psAppName );

//**************************************************************************************************

int  main( int argc, char * argv[ ] )
{
  wxString  osCmd;
  wxString  os1;

  // Validate the argument count passed to the application
  if( argc > 2 )           { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }

  // Process the command line arguments
  os1 = wxConvLibc.cMB2WC( argv[ 1 ] );
  if( argc > 1 )
  {
    if( os1 == wxT("-h") ) { Usage( argv[ 0 ] ); exit( EXIT_SUCCESS ); }
    else                   { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }
  }

  // Display the utility banner
  cout << "\n  Class CmdNgSpiceAC Test Utility"
       << "\n     Version 1.02 (12/08/2011)\n";

  // Create a GNU-CAP AC command object
  CmdNgSpiceAC  oCmd_AC;

  // Use the following command example to check the formatter and the parser :
  osCmd = wxT(".AC DEC 30 1.00K 300.00K");

  // Set things up for a formatter test
  oCmd_AC.m_osStart = wxT("1.00K");
  oCmd_AC.m_osStop  = wxT("300.00K");
  oCmd_AC.m_osStep  = wxT("30");
  oCmd_AC.m_eScale  = eSCALE_DEC;
  cout << "\nRun Formatter    : " << ( oCmd_AC.bFormat( ) ? "OK" : "FAULT" );
  cout << "\nTest Cmd Format  : " << ( oCmd_AC == osCmd   ? "OK" : "FAULT" );
  cout << "\nExample Command  : " << osCmd  .mb_str( );
  cout << "\noCmd_AC Contents : " << oCmd_AC.mb_str( ) << '\n';

  // Set things up for a parser test
  oCmd_AC.bSetString( osCmd );
  cout << "\nRun Parser       : " << ( oCmd_AC.bParse( ) ? "OK" : "FAULT" );
  oCmd_AC.bFormat( );
  cout << "\nTest Cmd Format  : " << ( oCmd_AC == osCmd  ? "OK" : "FAULT" );
  cout << "\nExample Command  : " << osCmd  .mb_str( );
  cout << "\noCmd_AC Contents : " << oCmd_AC.mb_str( ) << '\n';

  cout << '\n';

  exit( EXIT_SUCCESS );
}

//**************************************************************************************************

void  Usage( char * psAppName )
{
  cout << "\nUsage   : " << psAppName << " [-OPTIONS]"
       << "\nOptions :"
       << "\n  -h : Print usage (this message)\n";
}

#endif // TEST_CMDNGSPICEAC

//**************************************************************************************************

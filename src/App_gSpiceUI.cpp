//**************************************************************************************************
//                              App_gSpiceUI.hpp                                                   *
//                             ------------------                                                  *
// Started     : 2003-08-15                                                                        *
// Last Update : 2018-10-10                                                                        *
// Copyright   : (C) 2003-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "App_gSpiceUI.hpp"

//**************************************************************************************************
// All programs must have a "main" function. Under wxWidgets main( ) is implemented using the
// following macro which creates an application instance and starts program execution.

#if wxCHECK_VERSION( 3,0,0 )
wxIMPLEMENT_APP( App_gSpiceUI );
#else
wxIMPLEMENT_APP( App_gSpiceUI )
#endif

//**************************************************************************************************
// Implement an event table in which the events are routed to their respective
// handler functions in the class. If -1 is given as the ID, the given handler
// will be invoked for any event of the specified type.

//wxBEGIN_EVENT_TABLE( App_gSpiceUI, wxApp )


//wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.

App_gSpiceUI::App_gSpiceUI( void )
{
  g_bDebug = false;

  m_poFrmMain = NULL;
}

//**************************************************************************************************
// Destructor.

App_gSpiceUI::~App_gSpiceUI( )
{
}

//**************************************************************************************************
// Set the configuration file name and update its contents based on the command line arguments.

void  App_gSpiceUI::UpdateCfg( void )
{
  // Set the simulation engine type
  if( m_oCmdLn.m_eSimEng     != eSIMR_NONE ) g_oConfig.bSetSimEng( m_oCmdLn.m_eSimEng );

  // Set the schematic capture / editor program
  if( m_oCmdLn.m_eEdaSuite   != eEDA_NONE  ) g_oConfig.bSetEdaSuite( m_oCmdLn.m_eEdaSuite );

  // Set the waveform data viewer utility
  if( m_oCmdLn.m_eDataViewer != eVIEW_NONE ) g_oConfig.bSetDataViewer( m_oCmdLn.m_eDataViewer );

  // Set the analysis type
  if( m_oCmdLn.m_eAnalysis   != eCMD_NONE  ) g_oConfig.bSetAnalysis( m_oCmdLn.m_eAnalysis );

  // Set the Guile procedure to be used by GNetList
  if( ! m_oCmdLn.m_osGuileProc.IsEmpty( )  ) g_oConfig.bSetGuileProc( m_oCmdLn.m_osGuileProc );

  // Set the netlist and/or the schematics file name/s
  if( ! m_oCmdLn.m_osNetLstFile.IsEmpty( ) || ! m_oCmdLn.m_osaSchemFiles.IsEmpty( ) )
  {
      g_oConfig.bSetNetLstFile( m_oCmdLn.m_osNetLstFile  );
      g_oConfig.bSetSchemFiles( m_oCmdLn.m_osaSchemFiles );
  }

  // Write any changes to file
  g_oConfig.bFlush( );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// wxWidgets calls wxApp::OnInit( ) at startup.
//
// This method is used to initialize the application.
//
// Return Values :
//   true  - Success (continue  application execution)
//   false - Failure (terminate application execution)

bool  App_gSpiceUI::OnInit( void )
{
  // The following lines magically makes locale stuff work, don't touch!
  m_oLocale.Init( wxLANGUAGE_DEFAULT, wxLOCALE_LOAD_DEFAULT );
  setlocale( LC_NUMERIC, "C" );

  // Process the command line
  if( ! m_oCmdLn.bSetCmdLn( argc, argv ) ) return( false );
  if( ! m_oCmdLn.bProcArgs( ) )            return( false );

  // Open the global configuration object (this needs to happen as early as possible)
  if( ! m_oCmdLn.m_osConfigFile.IsEmpty( ) )
       g_oConfig.bSetFileName( m_oCmdLn.m_osConfigFile );
  else g_oConfig.bSetFileName( wxString( wxT('.') ) + GetAppName( ) + wxT(".conf") );
  g_oConfig.bOpen( );

  // Update the configuration with command line argument values (if any)
  UpdateCfg( );

  // Rebuild/clean the configuration file if requested
  if( m_oCmdLn.m_bCleanCfgFile ) g_oConfig.bClean( );

  // Create the GUI
  m_poFrmMain = new FrmMain( this );
  m_poFrmMain->Show( true );
  SetTopWindow( m_poFrmMain );

  // Display error message held over during startup
  m_poFrmMain->DlgErrMsg( );

  return( true );
}

//**************************************************************************************************
// wxWidgets calls wxApp::OnRun( ) to start execution of the application.
//
// This function is where the execution of a program written in wxWidgets starts. The default
// implementation just enters the main loop and starts handling the events until it terminates. The
// return value of this function becomes the exit code of the application.
//
// Return Values :
//   EXIT_SUCCESS - application exit code on success
//   EXIT_FAILURE - application exit code on failure
/*
int  App_gSpiceUI::OnRun( void )
{

  return( EXIT_SUCCESS );
}
*/
//**************************************************************************************************
// wxWidgets calls wxApp::OnExit( ) on application termination.
//
// This method does any processing which needs to be done as the application is about to exit. Note
// that it is not called at all if OnInit( ) failed.
//
// Return Values :
//   0 - the return value is not used

int  App_gSpiceUI::OnExit( void )
{
  g_oConfig.bFlush( );
  g_oConfig.bClose( );

  return( 0 );
}

//**************************************************************************************************

//**************************************************************************************************
//                                          ChoUnits.hpp                                           *
//                                         --------------                                          *
// Description : This class is intended to provide useful functionality for managing the units     *
//               associated with some variable. It is particular aimed at parameters to do with    *
//               electronics.                                                                      *
// Started     : 2004-03-27                                                                        *
// Last Update : 2018-10-18                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CHOUNITS_HPP
#define CHOUNITS_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/UnitsBase.hpp"

//**************************************************************************************************

class ChoUnits : public UnitsBase, public wxChoice
{
  private :

    eTypeUnits  m_eUnitsType;
    wxString    m_osDefUnits;

  public :

          ChoUnits( void );
         ~ChoUnits( );

    bool  bCreate   ( wxWindow * poWin, wxWindowID oWinID, int iWidth=-1 );
    bool  bIsCreated( void ) override { return( wxChoice::GetParent( )!=NULL ? true : false ); }

    bool  bClear( void );

    bool  bSetUnitsType( eTypeUnits eUType );
    bool  bSetUnits    ( const wxString & rosUnits );
    bool  bSetUnits    ( int iExp );
    bool  bSetDefUnits ( const wxString & rosUnits );

    inline        eTypeUnits   eGetUnitsType( void ) { return( m_eUnitsType ); }
            const wxString & rosGetUnits    ( void ) override;
                  int          iGetUnits    ( void ) override;
    inline  const wxString & rosGetDefUnits ( void ) { return( m_osDefUnits ); }

    void  Print( const wxString & rosPrefix=wxT("  ") );
};

//**************************************************************************************************

#endif // CHOUNITS_HPP

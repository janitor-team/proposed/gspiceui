//**************************************************************************************************
//                                          TextCtrl.cpp                                           *
//                                         --------------                                          *
// Started     : 2004-06-21                                                                        *
// Last Update : 2015-04-03                                                                        *
// Copyright   : (C) 2004-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "TextCtrl.hpp"

//**************************************************************************************************
// Initialize static variables for class

int  TextCtrl::m_iLinesMax = TXT_LNSDEF;
int  TextCtrl::m_iLinesDsp = TXT_DISPDEF;

//**************************************************************************************************
// Constructor.

TextCtrl::TextCtrl( void ) : wxTextCtrl( )
{
  bSetInitMsg( TXT_INITMSG );

  bClear( );
}

//**************************************************************************************************
// Destructor.

TextCtrl::~TextCtrl( )
{
}

//**************************************************************************************************
// Create an instantance of this object.
//
// Argument List :
//   poWin  - The parent window
//   oWinID - The window identifier
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bCreate( wxWindow * poWin, wxWindowID oWinID )
{
  long  lStyle;

  // Check if the object has already been created
  if( bIsCreated( ) )                                                               return( true );

  lStyle = wxTE_MULTILINE | wxTE_READONLY | wxSUNKEN_BORDER | wxTE_DONTWRAP |
           wxHSCROLL;
  if( ! Create( poWin, oWinID, wxT(""), wxDefaultPosition, wxDefaultSize, lStyle )) return( false );

  SetFont( FONT_MONO );

  return( bClear( ) );
}

//**************************************************************************************************
// Clear the text control.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bClear( void )
{
  long  lStyle;

  m_iLinesCnt = 0;

  // Check if the object has already been created
  if( ! bIsCreated( ) ) return( false );

  // Left-justify the text control contents
  lStyle = GetWindowStyle( );
  lStyle &= ~( wxTE_LEFT | wxTE_CENTER | wxTE_RIGHT );
  lStyle |= wxTE_LEFT;
  SetWindowStyle( lStyle );

  Clear( );              // Clear the text control contents
  SetEditable( false );  // Make the text control non-editable
  DiscardEdits( );       // Clear internal modified flag as if the current changes had been saved

  return( true );
}

//**************************************************************************************************
// Clear the text control and display a message to the user.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bInitialize( void )
{
  int  i1;

  // Check if the object has already been created
  if( ! bIsCreated( ) ) return( false );

  bClear( );        // Clear the text control contents

  if( ! m_osInitMsg.IsEmpty( ) )
  {
    // Center the default message horizontally
    SetWindowStyle( GetWindowStyle( ) | wxTE_CENTRE );

    // Display the requisite number of blank lines
    for( i1=1; i1<TXT_INITLNS; i1++ )
      AppendText( wxT("\n") );

    // Display the default message
    AppendText( m_osInitMsg );
  }

  DiscardEdits( );  // Clear internal modified flag as if the current changes had been saved

  return( true );
}

//**************************************************************************************************
// Is the text control empty (ie. contains nothing or just the initialization message).
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bIsEmpty( void )
{
  long  li1;

  if( wxTextCtrl::IsEmpty( ) )                                return( true );

  for( li1=1; li1<TXT_INITLNS; li1++ )
    if( ! GetLineText( li1-1 ).IsEmpty( ) )                   return( false );

  if( GetLineText( li1 ).Find( m_osInitMsg ) == wxNOT_FOUND ) return( false );

  return( true );
}

//**************************************************************************************************
// Set the initialization message to be displayed.
//
// Argument List :
//   rosMsg - The initialization message
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bSetInitMsg( const wxString & rosMsg )
{
  if( rosMsg.Length( ) > 60 ) return( false );

  m_osInitMsg = rosMsg;

  return( true );
}

//**************************************************************************************************
// Set the maximum number of lines to be loaded into the control.
//
// Argument List :
//   iLines - The maximum number of lines to be displayed
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bSetLinesMax( int iLines )
{
  if( iLines<TXT_LNSMIN || iLines>TXT_LNSMAX ) return( false );

  m_iLinesMax = iLines;

  return( true );
}

//**************************************************************************************************
// Set the number of lines to be displayed in the control.
//
// Argument List :
//   iLines - The number of lines to be displayed
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bSetLinesDsp( int iLines )
{
  if( iLines<TXT_DISPMIN || iLines>TXT_DISPMAX ) return( false );

  m_iLinesDsp = iLines;

  return( true );
}


//**************************************************************************************************
// Append a line of text to the text control.
//
// Argument List :
//   rosLine - The line of text to append
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bAppendLine( const wxString & rosLine )
{
  // Check if the object has already been created
  if( ! bIsCreated( ) )            return( false );

  // Check that the max. lines hasn't been reached
  if( m_iLinesCnt >= m_iLinesMax ) return( false );

  // Append this text and a line terminator
  *this << rosLine << wxT('\n');
  m_iLinesCnt++;

  return( true );
}

//**************************************************************************************************
// Append the contents of a file to the text control.
//
// Argument List :
//   roFName - The file name to append
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bAppendFile( const wxString & roFName )
{
  wxString  os1;
  size_t    szt1;

  // Check if the object has already been created
  if( ! bIsCreated( ) ) return( false );

  // Open the file
  wxTextFile  oFile( roFName );
  if( ! oFile.Open( ) )
  {
    *this << wxT("Couldn't load the file containing the process output :\n  ");
    if( roFName.IsEmpty( ) ) *this << wxT("The log file name is empty!");
    else                     *this << roFName;
    return( false );
  }

  // Check that the file isn't empty
  if( oFile.GetLineCount( ) > 0 )
  { // Append the file contents to the text control
    for( szt1=0; szt1<oFile.GetLineCount( ); szt1++ )
    {
      os1 = oFile.GetLine( szt1 );
      if( ! bAppendLine( os1 ) )
      {
        *this << wxT("\nText control maximum line count (ie. ") << m_iLinesMax
              << wxT(") reached.\n");
        break;
      }
    }
  }
  else *this << wxT("There is no process output.");

  oFile.Close( );    // Close the log file

  SetInsertionPoint( 0 ); // Go to the top of the text control

  return( true );
}

//**************************************************************************************************
// Load the contents of a file into the text control.
//
// Argument List :
//   roFName - The file name to load
//
// Return Values :
//   true  - Success
//   false - Failure

bool  TextCtrl::bLoadFile( const wxString & roFName )
{
  // Check if the object has already been created
  if( ! bIsCreated( ) ) return( false );

  bClear( ); // Clear the text control

  return( bAppendFile( roFName ) );
}

//**************************************************************************************************

#***************************************************************************************************
#                                             Makefile                                             *
#                                            ----------                                            *
# Description : Makefile for GNU SPICE GUI project house-keeping.                                  *
# Started     : 2004-01-08                                                                         *
# Last update : 2016-11-06                                                                         *
# Copyright   : (C) 2004 by MSWaters                                                               *
#***************************************************************************************************

#***************************************************************************************************
#                                                                                                  *
#       This program is free software; you can redistribute it and/or modify it under the          *
#       terms of the GNU General Public License as published by the Free Software Foundation;      *
#       either version 3 of the License, or (at your option) any later version.                    *
#                                                                                                  *
#***************************************************************************************************

#***************************************************************************************************
# Any or all of the values in this section may be set below or on the command line when envoking
# make eg. :
#
#   make DESTDIR=/new/dest/dir install
#   make DESTDIR=/new/dest/dir uninstall
#
# Note : Values specified on the command line over-ride those specified below.
#***************************************************************************************************

# Destination (install) directory
DESTDIR = /usr/local

#***************************************************************************************************
# Specify string values
#***************************************************************************************************

# Root directory
ROOT := $(shell pwd)

#***************************************************************************************************
# Make these targets
#***************************************************************************************************

all : config
	cd $(ROOT)/src ; $(MAKE)

#***************************************************************************************************
# Perform configuration tasks
#***************************************************************************************************

# Everyone should call this once
config :
ifneq ($(ROOT)/bin,$(wildcard $(ROOT)/bin))
	mkdir $(ROOT)/bin
endif
ifneq ($(ROOT)/src/obj,$(wildcard $(ROOT)/src/obj))
	mkdir $(ROOT)/src/obj
endif

# You should also call this once if running on a MAC
maccfg : $(ROOT)/mac/Info.plist $(ROOT)/mac/GSpiceUI.icns
ifneq ($(ROOT)/GSpiceUI.app,$(wildcard $(ROOT)/GSpiceUI.app))
	-mkdir $(ROOT)/GSpiceUI.app
	-mkdir $(ROOT)/GSpiceUI.app/Contents
	-mkdir $(ROOT)/GSpiceUI.app/Contents/MacOS
	-mkdir $(ROOT)/GSpiceUI.app/Contents/Resources
	cp $(ROOT)/mac/Info.plist $(ROOT)/GSpiceUI.app/Contents/
	echo -n 'APPL????' > $(ROOT)/GSpiceUI.app/Contents/PkgInfo
	cp $(ROOT)/mac/GSpiceUI.icns $(ROOT)/GSpiceUI.app/Contents/Resources/
endif

#***************************************************************************************************
# Create an archive of the essential project files as follows :
#
#   1. Change the make variable "TAR_NAME" (below) to the current version number
#   2. Change to the project root directory
#   3. Execute "make tar" to create the project archive file eg. gspiceui-v1.1.00.tar.gz
#***************************************************************************************************

TAR_VER=v1.2.36

TAR_DEST= gspiceui-$(TAR_VER)/gspiceui-$(TAR_VER).tar.gz

TAR_SRCS =gspiceui-$(TAR_VER)/release-notes-$(TAR_VER).txt
TAR_SRCS+=gspiceui-$(TAR_VER)/Authors
TAR_SRCS+=gspiceui-$(TAR_VER)/ChangeLog
TAR_SRCS+=gspiceui-$(TAR_VER)/Install
TAR_SRCS+=gspiceui-$(TAR_VER)/License
TAR_SRCS+=gspiceui-$(TAR_VER)/Makefile
TAR_SRCS+=gspiceui-$(TAR_VER)/ReadMe
TAR_SRCS+=gspiceui-$(TAR_VER)/ToDo
TAR_SRCS+=gspiceui-$(TAR_VER)/gspiceui.1
TAR_SRCS+=gspiceui-$(TAR_VER)/gspiceui.lsm
TAR_SRCS+=gspiceui-$(TAR_VER)/src/*
TAR_SRCS+=gspiceui-$(TAR_VER)/html/Makefile
TAR_SRCS+=gspiceui-$(TAR_VER)/html/*.html
TAR_SRCS+=gspiceui-$(TAR_VER)/html/*.png
TAR_SRCS+=gspiceui-$(TAR_VER)/html/*.jpg
TAR_SRCS+=gspiceui-$(TAR_VER)/sch/Makefile
TAR_SRCS+=gspiceui-$(TAR_VER)/sch/*/*.sch
TAR_SRCS+=gspiceui-$(TAR_VER)/lib/Makefile
TAR_SRCS+=gspiceui-$(TAR_VER)/lib/models/*.ckt
TAR_SRCS+=gspiceui-$(TAR_VER)/lib/models/*.mod
TAR_SRCS+=gspiceui-$(TAR_VER)/lib/symbols/*
TAR_SRCS+=gspiceui-$(TAR_VER)/mac/Info.plist
TAR_SRCS+=gspiceui-$(TAR_VER)/mac/GSpiceUI.icns
TAR_SRCS+=gspiceui-$(TAR_VER)/svg/Makefile
TAR_SRCS+=gspiceui-$(TAR_VER)/svg/*.svg

tar :
	make clean
	cd svg ; make ; cd ..
	cd .. ; mv gspiceui gspiceui-$(TAR_VER)
	cd .. ; tar --totals --exclude .svn -zcf $(TAR_DEST) $(TAR_SRCS)
	cd .. ; mv gspiceui-$(TAR_VER) gspiceui

#***************************************************************************************************
# Perform installation tasks
#***************************************************************************************************

install :
	mkdir -p     $(DESTDIR)/share/gspiceui
	cp ReadMe    $(DESTDIR)/share/gspiceui
	cp Install   $(DESTDIR)/share/gspiceui
	cp ChangeLog $(DESTDIR)/share/gspiceui
	mkdir -p     $(DESTDIR)/share/gspiceui/icons
	cp src/icons/gspiceui-*.xpm $(DESTDIR)/share/gspiceui/icons
	mkdir -p $(DESTDIR)/share/man/man1
	cp gspiceui.1 $(DESTDIR)/share/man/man1
	cd $(ROOT)/src  ; $(MAKE) install DESTDIR=$(DESTDIR)
	cd $(ROOT)/html ; $(MAKE) install DESTDIR=$(DESTDIR)
	cd $(ROOT)/sch  ; $(MAKE) install DESTDIR=$(DESTDIR)
	cd $(ROOT)/lib  ; $(MAKE) install DESTDIR=$(DESTDIR)

#***************************************************************************************************
# Perform uninstall tasks
#***************************************************************************************************

uninstall :
	cd $(ROOT)/src  ; $(MAKE) uninstall DESTDIR=$(DESTDIR)
	cd $(ROOT)/html ; $(MAKE) uninstall DESTDIR=$(DESTDIR)
	cd $(ROOT)/sch  ; $(MAKE) uninstall DESTDIR=$(DESTDIR)
	cd $(ROOT)/lib  ; $(MAKE) uninstall DESTDIR=$(DESTDIR)
	rm -f  $(DESTDIR)/share/man/man1/gspiceui.1
	rm -fr $(DESTDIR)/share/gspiceui
#	rmdir --ignore-fail-on-non-empty $(DESTDIR)/share/man/man1
#	rmdir --ignore-fail-on-non-empty $(DESTDIR)/share/man
#	rmdir --ignore-fail-on-non-empty $(DESTDIR)/share

#***************************************************************************************************
# Remove temporary files and backup files
#***************************************************************************************************

clean :
	rm -f Makefile~ charcnt~
	cd $(ROOT)/src  ; $(MAKE) cleanall
	cd $(ROOT)/html ; $(MAKE) clean
	cd $(ROOT)/sch  ; $(MAKE) clean
	cd $(ROOT)/lib  ; $(MAKE) clean

#***************************************************************************************************
# Calculate the size of the various sources making up this project
#***************************************************************************************************

size :
	$(ROOT)/charcnt.sh >> charcnt.txt

#***************************************************************************************************
# Display a help message
#***************************************************************************************************

help :
	@echo
	@echo -e "gSpiceUI project build system. "
	@echo
	@echo -e "Available make targets :"
	@echo -e "  all       - Build the config target (default)"
	@echo -e "  config    - Configure the build system"
	@echo -e "  tar       - Build a tar file of the project sources"
	@echo -e "  install   - Perform project wide   install tasks"
	@echo -e "  uninstall - Perform project wide uninstall tasks"
	@echo -e "  clean     - Remove temporary files and backup files"
	@echo -e "  size      - Log source sizes to file charcnt.txt"
	@echo -e "  help      - Display this message"
	@echo

#***************************************************************************************************
# Specify phony targets
#***************************************************************************************************

.PHONY : config tar install uninstall clean size help

#***************************************************************************************************
